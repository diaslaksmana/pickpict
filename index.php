<?php
include "header.php";
?>
<?php
include "header2.php";
?>

<div id="about" class="about">
	<div class="container">
		<div class="icon-pickpict" align="center">
			<img src="assets/img/icon/logo2.png">
		</div>
		<div class="description-pickpict mt-5">
			<p align="center">
				Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem
			</p>
		</div>

		<div class="mt-5" align="center">
			<a href="" class="brk-btn">
				Pesan Sekarang
			</a>
		</div>
		
	</div>

	<div class="img-about"></div>
</div>

<div id="index-service">
	<div class="container">
		<div class="title py-4">
			<h1><u>Serv</u>ice</h1>
		</div>
		<div class="row">
			<div class="col-12 col-sm-12 col-md-6 col-lg-6">
				<div class="description-service">
					<p>
						Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem <br><br>
						Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem
					</p>
				</div>
			</div>
			<div class="col-12 col-sm-12 col-md-6 col-lg-6">
				<div class="img-icon-service">
					<img src="assets/img/service.svg">
				</div>
			</div>
		</div>
	</div>
</div>


<div id="index-koleksi">
	<div class="container">
		<div class="title py-4">
			<h1><u>Kole</u>ksi</h1>
		</div>
		<div class="row">
			<div class="col-12 col-sm-12 col-md-6 col-lg-6">
				<div class="img-koleksi">
					<img src="assets/img/koleksi.svg">
				</div>
			</div>
			<div class="col-12 col-sm-12 col-md-6 col-lg-6">				
				<div class="description-koleksi">
					<p>
						Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem <br><br>
						Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem
					</p>
				</div>
			</div>
		</div>
	</div>
</div>


<div id="why-pickpict">
	<div class="container">
		<div class="title py-4">
			<h1>
				Mengapa Pickpict ?
			</h1>
		</div>
		<div class="row">
			<div class="col-12 col-sm-12 col-md-12 col-lg-4">
				<div class="box-description bg-color-blue">
					<p>
						Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem
					</p>					
				</div>
			</div>
			<div class="col-12 col-sm-12 col-md-12 col-lg-4">
				<div class="box-description bg-color-blue">
					<p>
						Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem
					</p>
				</div>
			</div>
			<div class="col-12 col-sm-12 col-md-12 col-lg-4">
				<div class="box-description bg-color-blue">
					<p>
						Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem
					</p>
				</div>
			</div>
		</div>
	</div>
</div>


<?php
include "footer2.php";
?>
<?php
include "footer.php";
?>