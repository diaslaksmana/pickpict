<?php
include "header.php";
?>
<?php
include "header-dashboard.php";
?>

<div id="dashboard" class="dashboard bg-grey">
	<div class="container">
		<div class="bg-white py-3 px-3 b-r-5">
			<div class="form-row border-bottom">
				<div class="form-group col-md-6 ">
					<h6 class="b-600">Upload Foto</h6>
					<div class="main-wrapper">
						<div class="img-upload-plugin">
							<div class="img-upload-handler">
								<div class="img-preview-big">
									<img src="https://uploader-assets.s3.ap-south-1.amazonaws.com/codepen-default-placeholder.png">
									<div class="img-delete">
										<img src="https://uploader-assets.s3.ap-south-1.amazonaws.com/codepen-delete-icon.png">
									</div>
								</div>
							</div>
							<div class="img-preview-operate">
								<div class="img-holder" style="display: inline-block; padding: 2px;"></div>
								<button class="img-add-more">
									<img src="https://uploader-assets.s3.ap-south-1.amazonaws.com/codepen-add-more-btn.png">
								</button>
							</div>
							<input type="file" name="img-upload-input" style="display:none">
						</div>
					</div>
				</div>
			</div>
			<div class="form-row border-bottom mt-2">
				<div class="form-group  col-md-6">
					<h6 class="b-600">Judul</h6>
					<div class="">
						<input type="text" class="form-control form-control-sm" id="" placeholder="Judul Ilustrasi">
					</div>						
				</div>
			</div>
			<div class="form-row border-bottom mt-2">
				<div class="form-group col-md-6">
					<h6 class="b-600">Deskripsi</h6>
					<div class="">
						<textarea class="form-control form-control-sm" id="" rows="5" placeholder="Tuliskan deskripsi ilustrasi"></textarea>
					</div>						
				</div>
			</div>
			<div class="form-row border-bottom mt-2">
				<div class="form-group  col-md-6">
					<h6 class="b-600">Tag</h6>
					<div class="">
						<input type="text" class="form-control form-control-sm" id="" placeholder="Tag">
					</div>						
				</div>
			</div>
			<div class="form-row border-bottom mt-2">
				<div class="form-group col-md-6">
					<h6>Pilih Produk</h6>
					<div class="form-check">
						<input type="checkbox" name="colorCheckbox" value="bantal">
						<label class="form-check-label" for="">
							Bantal
						</label>
					</div>
					<div class="form-check">
						<input type="checkbox" name="colorCheckbox" value="mug">
						<label class="form-check-label" for="">
							MUG
						</label>
					</div>
					<div class="form-check">
						<input type="checkbox" name="colorCheckbox" value="kaos">
						<label class="form-check-label" for="">
							Kaos
						</label>
					</div>
					<div class="form-check">
						<input type="checkbox" name="colorCheckbox" value="pigura">
						<label class="form-check-label" for="">
							Pigura
						</label>
					</div>
					<div class="form-check">
						<input type="checkbox" name="colorCheckbox" value="case">
						<label class="form-check-label" for="">
							Case HP
						</label>
					</div>				
				</div>
			</div>

			<!--list koleksi-->
			<div class="list_produk2">

				<div class="bantal box-ilustrasi">
					<h6 class="b-600 mt-2">Bantal</h6>
					<div class="row scroll mb-2">
						<div class="col-3 col-sm-2 col-md-2 col-lg-2">
							<label>Ukuran</label>
							<h6>30x30 cm</h6>
							<h6>40x40 cm</h6>
						</div>
						<div class="col-2 col-sm-2 col-md-3 col-lg-2">
							<label>Harga Dasar</label>
							<h6>Rp. 50.000</h6>
							<h6>Rp. 75.000</h6>
						</div>
						<div class="col-3 col-sm-2 col-md-2 col-lg-2">
							<label>Keuntunganmu</label>
							<div class="list3" style="margin-bottom: 5px;">
								<input type="email" class="form-control form-control-sm" id="" aria-describedby="" min="0" placeholder="">
							</div>
							<div class="list3" style="margin-bottom: 5px;">
								<input type="email" class="form-control form-control-sm" id="" aria-describedby="" min="0" placeholder="">
							</div>
						</div>
						<div class="col-2 col-sm-2 col-md-2 col-lg-2">
							<label>Harga Jual</label>
							<h6>Rp. 50.000</h6>
							<h6>Rp. 75.000</h6>
						</div>
						<div class="col-2 col-sm-2 col-md-2 col-lg-2">
							<img src="assets/img/bantal.jpg" width="100%">
						</div>
					</div>
				</div>

				<div class="mug box-ilustrasi">
					<h6 class="b-600 mt-2">MUG</h6>
					<div class="row  scroll mb-2" >
						<div class="col-3 col-sm-6 col-md-2 col-lg-2">
							<label>Ukuran</label>
							<h6>30x30 cm</h6>
							<h6>40x40 cm</h6>
						</div>
						<div class="col-2 col-sm-6 col-md-3 col-lg-2">
							<label>Harga Dasar</label>
							<h6>Rp. 50.000</h6>
							<h6>Rp. 75.000</h6>
						</div>
						<div class="col-3 col-sm-6 col-md-2 col-lg-2">
							<label>Keuntunganmu</label>
							<div class="list3" style="margin-bottom: 5px;">
								<input type="email" class="form-control form-control-sm" id="" aria-describedby="" min="0" placeholder="">
							</div>
							<div class="list3" style="margin-bottom: 5px;">
								<input type="email" class="form-control form-control-sm" id="" aria-describedby="" min="0" placeholder="">
							</div>
						</div>
						<div class="col-2 col-sm-6 col-md-2 col-lg-2">
							<label>Harga Jual</label>
							<h6>Rp. 50.000</h6>
							<h6>Rp. 75.000</h6>
						</div>
						<div class="col-2 col-sm-6 col-md-2 col-lg-2">
							<img src="assets/img/mug.jpeg" width="100%">
						</div>
					</div>
				</div>

				<div class="kaos box-ilustrasi">
					<h6 class="b-600 mt-2">Kaos</h6>
					<div class="row  scroll mb-2">
						<div class="col-3 col-sm-6 col-md-2 col-lg-2">
							<label>Ukuran</label>
							<h6>S</h6>
							<h6>M</h6>
							<h6>L</h6>
							<h6>XL</h6>
						</div>
						<div class="col-2 col-sm-6 col-md-3 col-lg-2">
							<label>Harga Dasar</label>
							<h6>Rp. 100.000</h6>
							<h6>Rp. 100.000</h6>
							<h6>Rp. 100.000</h6>
							<h6>Rp. 100.000</h6>
						</div>
						<div class="col-3 col-sm-6 col-md-2 col-lg-2">
							<label>Keuntunganmu</label>
							<div class="list3" style="margin-bottom: 5px;">
								<input type="email" class="form-control form-control-sm" id="" aria-describedby="" min="0" placeholder="">
							</div>
							<div class="list3" style="margin-bottom: 5px;">
								<input type="email" class="form-control form-control-sm" id="" aria-describedby="" min="0" placeholder="">
							</div>
							<div class="list3" style="margin-bottom: 5px;">
								<input type="email" class="form-control form-control-sm" id="" aria-describedby="" min="0" placeholder="">
							</div>
							<div class="list3" style="margin-bottom: 5px;">
								<input type="email" class="form-control form-control-sm" id="" aria-describedby="" min="0" placeholder="">
							</div>
						</div>
						<div class="col-2 col-sm-6 col-md-2 col-lg-2">
							<label>Harga Jual</label>
							<h6>Rp. 150.000</h6>
							<h6>Rp. 150.000</h6>
							<h6>Rp. 150.000</h6>
							<h6>Rp. 150.000</h6>
						</div>
						<div class="col-2 col-sm-6 col-md-2 col-lg-2">
							<img src="assets/img/s2.jpg" width="100%">
						</div>
					</div>
				</div>

				<div class="pigura box-ilustrasi">
					<h6 class="b-600 mt-2">Pigura</h6>
					<div class="row  scroll mb-2" >
						<div class="col-3 col-sm-6 col-md-2 col-lg-2">
							<label>Ukuran</label>
							<h6>30x30 cm</h6>
							<h6>30x40 cm</h6>
							<h6>30x50 cm</h6>
						</div>
						<div class="col-2 col-sm-6 col-md-3 col-lg-2">
							<label>Harga Dasar</label>
							<h6>Rp. 100.000</h6>
							<h6>Rp. 100.000</h6>
							<h6>Rp. 100.000</h6>
						</div>
						<div class="col-3 col-sm-6 col-md-2 col-lg-2">
							<label>Keuntunganmu</label>
							<div class="list3" style="margin-bottom: 5px;">
								<input type="email" class="form-control form-control-sm" id="" aria-describedby="" min="0" placeholder="">
							</div>
							<div class="list3" style="margin-bottom: 5px;">
								<input type="email" class="form-control form-control-sm" id="" aria-describedby="" min="0" placeholder="">
							</div>
							<div class="list3" style="margin-bottom: 5px;">
								<input type="email" class="form-control form-control-sm" id="" aria-describedby="" min="0" placeholder="">
							</div>
						</div>
						<div class="col-2 col-sm-6 col-md-2 col-lg-2">
							<label>Harga Jual</label>
							<h6>Rp. 150.000</h6>
							<h6>Rp. 150.000</h6>
							<h6>Rp. 150.000</h6>
						</div>
						<div class="col-2 col-sm-6 col-md-2 col-lg-2">
							<img src="assets/img/wpap2.jpg" width="100%">
						</div>
					</div>
				</div>

				<div class="case box-ilustrasi">
					<h6 class="b-600 mt-2">Case</h6>
					<div class="row  scroll mb-2" >
						<div class="col-3 col-sm-6 col-md-2 col-lg-2">
							<label>Type</label>
							<h6>-</h6>
						</div>
						<div class="col-2 col-sm-6 col-md-3 col-lg-2">
							<label>Harga Dasar</label>
							<h6>Rp. 100.000</h6>
						</div>
						<div class="col-3 col-sm-6 col-md-2 col-lg-2">
							<label>Keuntunganmu</label>
							<div class="list3" style="margin-bottom: 5px;">
								<input type="email" class="form-control form-control-sm" id="" aria-describedby="" min="0" placeholder="">
							</div>
						</div>
						<div class="col-2 col-sm-6 col-md-2 col-lg-2">
							<label>Harga Jual</label>
							<h6>Rp. 150.000</h6>
						</div>
						<div class="col-2 col-sm-6 col-md-2 col-lg-2">
							<img src="assets/img/casehp.jpg" width="100%">
						</div>
					</div>
				</div>

			</div>

			<div class=" btn-yellow btn-detail mt-3" align="center">
				<a class=" bg-pink py-2 btn-sm mr-2" href="portofolio.php" role="button">Batalkan</a>
				<button class="btn btn-pink btn-sm" type="submit">Publikasikan</button>
			</div>

		</div>
		
	</div>
</div>

<?php
include "footer-dashboard.php";
?>
<?php
include "footer.php";
?>