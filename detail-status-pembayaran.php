<?php
include "header.php";
?>
<?php
include "header-dashboard.php";
?>

<div id="dashboard" class="dashboard bg-grey">
	<div class="container">
		<h5 class="b-600">Detail Pembelian</h5>
		<div class="bg-white py-4 px-2 b-r-5">
			<h6>Pesanan Anda</h6>
			<div class="mb-4 pb-3 border-bottom">
				<div class="row">
					<div class="col-12 col-sm-12 col-md-6 col-lg-3 col-xl-2">
						<div class="img-order">
							<img src="assets/img/profil3.jpg">
						</div>

					</div>
					<div class="col-12 col-sm-12 col-md-6 col-lg-4 col-xl-4 mb-4">
						<h5>Rincian :</h5>
						<h6>WPAP Wajah</h6>
						<h6>Harga : <b> Rp. 150.000,00 </b></h6>
						<h6>Tipe : Digital</h6>						
						<h6>Tambah Subjek : 1</h6>
						<h6>Warna : Full Color</h6>
						<h6>Proporsi : Close-up</h6>
						<h6>Jenis FIle : JPG</h6>
						<h6>Lama Pengerjaan : 5 Hari, (1 Januari 2018)</h6>	
					</div>
					<div class="col-12 col-sm-12 col-md-6 col-lg-3 col-xl-3">
						<h5>Cetak ke produk :</h5>
						<h6>Jenis Produk : -</h6>
						<h6>Ukuran : -</h6>
						<h6>Warna : -</h6>
					</div>
				</div>
			</div>		

			<div class="mb-4 pb-3 border-bottom">
				<div class="row">
					<div class="col-12 col-sm-12 col-md-6 col-lg-3 col-xl-2">
						<div class="img-order">
							<img src="assets/img/profil4.jpg">
						</div>

					</div>
					<div class="col-12 col-sm-12 col-md-6 col-lg-4 col-xl-4 mb-4">
						<h5>Rincian :</h5>
						<h6>WPAP Wajah</h6>
						<h6>Harga : <b> Rp. 150.000,00 </b></h6>
						<h6>Tipe : Digital</h6>						
						<h6>Tambah Subjek : 1</h6>
						<h6>Warna : Full Color</h6>
						<h6>Proporsi : Close-up</h6>
						<h6>Jenis FIle : JPG</h6>
						<h6>Lama Pengerjaan : 5 Hari, (1 Januari 2018)</h6>	
					</div>
					<div class="col-12 col-sm-12 col-md-6 col-lg-3 col-xl-3">
						<h5>Cetak ke produk :</h5>
						<h6>Jenis Produk : Baju</h6>
						<h6>Ukuran : L</h6>
						<h6>Warna : Hitam</h6>
					</div>
				</div>
			</div>	

			<div class="mb-3">
				<h5>Total Pembayaran : Rp. 400.000,00</h5>		
				<h6>Status : Baru</h6>	
			</div>	

			<div class="btn-detail" align="center">
				<a class=" bg-yellow btn-sm mr-2" href="pembelian.php" role="button">Kembali</a>
				<a class=" bg-blue btn-sm" href="info-pembayaran.php" role="button">Bayar Sekarang</a>
			</div>

		</div>
		
	</div>
</div>

<?php
include "footer-dashboard.php";
?>
<?php
include "footer.php";
?>