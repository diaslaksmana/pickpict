<?php
include "header.php";
?>
<?php
include "header-dashboard.php";
?>

<div id="dashboard" class="dashboard bg-grey">
	<div class="container">
		<div class="bg-white py-3 px-3 b-r-5">
			<div class="form-row border-bottom">
				<div class="form-group col-md-6">
					<h6 class="b-600">Judul</h6>
					<div class="">
						<input type="text" class="form-control form-control-sm" id="" placeholder="Judul Ilustrasi">
					</div>						
				</div>
			</div>
			<div class="form-row border-bottom mt-2 select-ilustration">
				<div class="form-group col-md-6">
					<h6 class="b-600">Pilih Kategori</h6>
					<div class="form-check">
						<input type="radio" id="radio01" name="radio" />
						<label for="radio01"><span></span>WPAP</label>
					</div>
					<div class="form-check">
						<input type="radio" id="radio02" name="radio" />
						<label for="radio02"><span></span>Vektor Wajah</label>
					</div>
					<div class="form-check">
						<input type="radio" id="radio03" name="radio" />
						<label for="radio03"><span></span>Kartun</label>
					</div>
					<div class="form-check">
						<input type="radio" id="radio04" name="radio" />
						<label for="radio04"><span></span>Karikatur</label>
					</div>
					<div class="form-check">
						<input type="radio" id="radio05" name="radio" />
						<label for="radio05"><span></span>POP Art</label>
					</div>
					<div class="form-check">
						<input type="radio" id="radio06" name="radio" />
						<label for="radio06"><span></span>Water Color</label>
					</div>					
				</div>
			</div>
			<div class="form-row border-bottom mt-2">
				<div class="form-group col-md-6">
					<h6 class="b-600">Deskripsi</h6>
					<div class="list3">
						<textarea class="form-control" id="" rows="5" placeholder="Tuliskan deskripsi ilustrasi"></textarea>
					</div>						
				</div>
			</div>
			<div class="form-row border-bottom mt-2">
				<div class="form-group col-md-6 ">
					<h6 class="b-600 mb-2">Fitur yang didapatkan pelanggan</h6>
					<div class="row fitur">
						<div class="col-4 col-sm-4 col-md-4 col-lg-4">
							<h6>Jumlah Subjek</h6>
						</div>
						<div class="col-8 col-sm-8 col-md-8 col-lg-8">
							<div class="switch-field">
								<input type="radio" id="switch_3_left" name="switch_3" value="1" />
								<label for="switch_3_left">1</label>
								<input type="radio" id="switch_3_center" name="switch_3" value="2" />
								<label for="switch_3_center">2</label>
								<input type="radio" id="switch_3_right" name="switch_3" value="3" />
								<label for="switch_3_right">3</label>
							</div>
						</div>
					</div>
					<div class="row fitur">
						<div class="col-4 col-sm-4 col-md-4 col-lg-4">
							<h6>Warna</h6>
						</div>
						<div class="col-8 col-sm-8 col-md-8 col-lg-8">
							<div class="switch-field">
								<input type="radio" id="switch_left" name="switch_2" value="color" />
								<label for="switch_left">full Color</label>
								<input type="radio" id="switch_right" name="switch_2" value="black_white" />
								<label for="switch_right">Black and white</label>
							</div>
						</div>
					</div>
					<div class="row fitur">
						<div class="col-4 col-sm-4 col-md-4 col-lg-4">
							<h6>Proporsi</h6>
						</div>
						<div class="col-8 col-sm-8 col-md-8 col-lg-8">
							<div class="switch-field">
								<input type="radio" id="Close-up" name="switch_1" value="Close-up" />
								<label for="Close-up">Close-up</label>
								<input type="radio" id="full-body" name="switch_1" value="full-body" />
								<label for="full-body">Full Body</label>
							</div>
						</div>
					</div>
					<div class="row fitur">
						<div class="col-4 col-sm-4 col-md-4 col-lg-4">
							<h6>Jenis File</h6>
						</div>
						<div class="col-8 col-sm-8 col-md-8 col-lg-8">
							<div class="switch-field">
								<input type="radio" id="pdf" name="switch_4" value="" />
								<label for="pdf">PDF</label>
								<input type="radio" id="jpg" name="switch_4" value="" />
								<label for="jpg">JPG</label>
								<input type="radio" id="esp" name="switch_4" value="" />
								<label for="esp">ESP</label>
							</div>
						</div>
					</div>

				</div>
			</div>
			<div class="row border-bottom mt-2">
				<div class="col-12 col-sm-12 col-md-6 col-lg-6">
					<div class="form-group">
						<h6 class="b-600">Harga</h6>
						<div class="list3">
							<input type="number" min="0" class="form-control form-control-sm" id="" placeholder="Harga dasar"> 
						</div>						
					</div>
				</div>
				<div class="col-12 col-sm-12 col-md-6 col-lg-6">
					<div class="form-group">
						<h6 class="b-600">Durasi pengerjaan</h6>
						<div class="list3">
							<select class="form-control form-control-sm" id="">
								<option>1 Hari</option>
								<option>2 Hari</option>
								<option>3 Hari</option>
								<option>4 Hari</option>
								<option>5 Hari</option>
							</select>
						</div>						
					</div>
				</div>
			</div>
			<div class=" border-bottom mt-2">
				<h6 class="b-600">Sampel ilustrasi</h6>
				<p class="grey"><i>Kamu bisa menambahkan 3 sampel ilustrasi</i></p>
				<div class="main-wrapper">
					<div class="img-upload-plugin">
						<div class="img-upload-handler">
							<div class="img-preview-big">
								<img src="https://uploader-assets.s3.ap-south-1.amazonaws.com/codepen-default-placeholder.png">
								<div class="img-delete">
									<img src="https://uploader-assets.s3.ap-south-1.amazonaws.com/codepen-delete-icon.png">
								</div>
							</div>
						</div>
						<div class="img-preview-operate">
							<div class="img-holder" style="display: inline-block; padding: 2px;"></div>
							<button class="img-add-more">
								<img src="https://uploader-assets.s3.ap-south-1.amazonaws.com/codepen-add-more-btn.png">
							</button>
						</div>
						<input type="file" name="img-upload-input" style="display:none">
					</div>
				</div>
			</div>
			<div class="border-bottom mt-2">
				<div class="form-row ">
					<div class="form-group col-md-12 col-lg-12 col-xl-6">
						<h6 class="b-600">Tambah Fitur</h6>
						<div class="row ">
							<div class="col-12 col-sm-4 col-md-4 col-lg-4">
								<label>Nama Fitur</label>
								<div class="list3">
									<input type="text" class="form-control form-control-sm" id="" placeholder="Nama Fitur"> 
								</div>								
							</div>
							<div class="col-12 col-sm-4 col-md-4 col-lg-4">
								<label>Tambahan waktu</label>
								<div class="list3">
									<select class="form-control form-control-sm" id="">
										<option>1 Hari</option>
										<option>2 Hari</option>
										<option>3 Hari</option>
										<option>4 Hari</option>
										<option>5 Hari</option>
									</select>
								</div>	
							</div>
							<div class="col-12 col-sm-4 col-md-4 col-lg-4">
								<label>Harga</label>
								<div class="list3">
									<input type="number" min="0" class="form-control form-control-sm" id="" placeholder="Harga"> 
								</div>
							</div>
						</div>						
					</div>
				</div>
				<div class="form-group btn-blue col-md-6" align="right">
					<button type="button" class="btn btn-biru btn-sm">Tambah</button>
				</div>
			</div>

			<div class=" btn-yellow btn-detail mt-3" align="left">
				<a class=" bg-pink py-2 btn-sm mr-2" href="portofolio.php" role="button">Batalkan</a>
				<button class="btn btn-pink btn-sm" type="submit">Publikasikan</button>
			</div>

		</div>
		
	</div>
</div>

<?php
include "footer-dashboard.php";
?>
<?php
include "footer.php";
?>