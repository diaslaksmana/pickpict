<?php
include "header.php";
?>
<?php
include "header-dashboard.php";
?>

<div id="dashboard" class="dashboard bg-grey">
	<div class="container">

		<div class="bg-white py-2 px-2 b-r-5">
			<!--Service-->

			<div class="row pt-2">
				<div class="col-6 coll-sm-6 col-md-6">
					<h5><b>Service </b></h5>
				</div>
				<div class="col-6 coll-sm-6 col-md-6" align="right">
					<div class="btn-view-all">
						<a href="upload-service-ilustrasi.php">Tambah</a>
					</div>
				</div>
			</div>

			<div class="row mt-3">
				<div class="col-12 col-sm-6 col-md-6 col-lg-3">
					<div class="postcard">					
						<div class="cover100">
							<img src="assets/img/kaos.jpg">
						</div>
						<div class="postcard-body">
							<a href="detail-produk.php">
								<h5>John Lennon</h5>
								<h6>WPAP</h6>
								<small>by John Dae</small>
							</a>						
						</div>
					</div>
				</div>
				<div class="col-12 col-sm-6 col-md-6 col-lg-3">
					<div class="postcard">					
						<div class="cover100">
							<img src="assets/img/mug.jpeg">
						</div>
						<div class="postcard-body">
							<a href="">
								<h5>Taylor Swift</h5>
								<h6>Mug</h6>
								<small>by John Dae</small>
							</a>						
						</div>
					</div>
				</div>
				<div class="col-12 col-sm-6 col-md-6 col-lg-3">
					<div class="postcard">					
						<div class="cover100">
							<img src="assets/img/bantal.jpg">
						</div>
						<div class="postcard-body">
							<a href="">
								<h5>Women</h5>
								<h6>Bantal</h6>
								<small>by John Dae</small>
							</a>						
						</div>
					</div>
				</div>
				<div class="col-12 col-sm-6 col-md-6 col-lg-3">
					<div class="postcard">					
						<div class="cover100">
							<img src="assets/img/casehp.jpg">
						</div>
						<div class="postcard-body">
							<a href="">
								<h5>OASIS</h5>
								<h6>WPAP</h6>
								<small>by John Dae</small>
							</a>						
						</div>
					</div>
				</div>
			</div>

			<div class="page-number">
				<nav aria-label="Page navigation ">
					<ul class="pagination justify-content-center">
						<li class="disabled">
							<a class="" href="#" tabindex="-1" aria-disabled="true">
								<i class="fas fa-chevron-circle-left"></i>
							</a>
						</li>
						<li class="active"><a class="" href="#">1</a></li>
						<li class=""><a class="" href="#">2</a></li>
						<li class=""><a class="" href="#">3</a></li>
						<li class=""><a class="" href="#">4</a></li>
						<li class=""><a class="" href="#">5</a></li>
						<li class="">
							<a class="" href="#">
								<i class="fas fa-chevron-circle-right"></i>
							</a>
						</li>
					</ul>
				</nav>	
			</div>
		</div>


		<div class="bg-white py-2 px-2 b-r-5 mt-4">
			<!--Koleksi-->

			<div class="row pt-2">
				<div class="col-6 coll-sm-6 col-md-6">
					<h5><b>Koleksi </b></h5>
				</div>
				<div class="col-6 coll-sm-6 col-md-6" align="right">
					<div class="btn-view-all">
						<a href="upload-koleksi.php">Tambah</a>
					</div>
				</div>
			</div>

			<div class="row mt-3">
				<div class="col-12 col-sm-6 col-md-6 col-lg-3">
					<div class="postcard">					
						<div class="cover100">
							<span>
								Rp 120.000
							</span>
							<img src="assets/img/kaos.jpg">
						</div>
						<div class="postcard-body">
							<a href="detail-produk.php">
								<h5>John Lennon</h5>
								<h6>WPAP</h6>
								<small>by John Dae</small>
							</a>						
						</div>
						<div class="postcard-footer">
							<label>
								<i class="far fa-eye"></i> 100
							</label>
							<label>
								<i class="fas fa-heart"></i> 50
							</label>
							<label>
								<i class="fab fa-twitch"></i> 80
							</label>
						</div>
					</div>
				</div>
				<div class="col-12 col-sm-6 col-md-6 col-lg-3">
					<div class="postcard">					
						<div class="cover100">
							<span>
								Rp 50.000
							</span>
							<img src="assets/img/mug.jpeg">
						</div>
						<div class="postcard-body">
							<a href="">
								<h5>Taylor Swift</h5>
								<h6>Mug</h6>
								<small>by John Dae</small>
							</a>						
						</div>
						<div class="postcard-footer">
							<label>
								<i class="far fa-eye"></i> 100
							</label>
							<label>
								<i class="fas fa-heart"></i> 50
							</label>
							<label>
								<i class="fab fa-twitch"></i> 80
							</label>
						</div>
					</div>
				</div>
				<div class="col-12 col-sm-6 col-md-6 col-lg-3">
					<div class="postcard">					
						<div class="cover100">
							<span>
								Rp 100.000
							</span>
							<img src="assets/img/bantal.jpg">
						</div>
						<div class="postcard-body">
							<a href="">
								<h5>Women</h5>
								<h6>Bantal</h6>
								<small>by John Dae</small>
							</a>						
						</div>
						<div class="postcard-footer">
							<label>
								<i class="far fa-eye"></i> 100
							</label>
							<label>
								<i class="fas fa-heart"></i> 50
							</label>
							<label>
								<i class="fab fa-twitch"></i> 80
							</label>
						</div>
					</div>
				</div>
				<div class="col-12 col-sm-6 col-md-6 col-lg-3">
					<div class="postcard">					
						<div class="cover100">
							<span>
								Rp 120.000
							</span>
							<img src="assets/img/casehp.jpg">
						</div>
						<div class="postcard-body">
							<a href="">
								<h5>OASIS</h5>
								<h6>WPAP</h6>
								<small>by John Dae</small>
							</a>						
						</div>
						<div class="postcard-footer">
							<label>
								<i class="far fa-eye"></i> 100
							</label>
							<label>
								<i class="fas fa-heart"></i> 50
							</label>
							<label>
								<i class="fab fa-twitch"></i> 80
							</label>
						</div>
					</div>
				</div>
			</div>

			<div class="page-number">
				<nav aria-label="Page navigation ">
					<ul class="pagination justify-content-center">
						<li class="disabled">
							<a class="" href="#" tabindex="-1" aria-disabled="true">
								<i class="fas fa-chevron-circle-left"></i>
							</a>
						</li>
						<li class="active"><a class="" href="#">1</a></li>
						<li class=""><a class="" href="#">2</a></li>
						<li class=""><a class="" href="#">3</a></li>
						<li class=""><a class="" href="#">4</a></li>
						<li class=""><a class="" href="#">5</a></li>
						<li class="">
							<a class="" href="#">
								<i class="fas fa-chevron-circle-right"></i>
							</a>
						</li>
					</ul>
				</nav>	
			</div>
		</div>
	</div>
</div>

<?php
include "footer-dashboard.php";
?>
<?php
include "footer.php";
?>