<?php
include "header.php";
?>
<?php
include "header-dashboard.php";
?>

<div id="dashboard" class="dashboard bg-grey">
	<div class="container">
		<h5>Rincian Pesanan</h5>
		<div class="bg-white py-3 px-3">
			<div class="row">
				<div class="col-12 col-sm-6 col-md-6 col-lg-6">
					<div>
						<div class="img3 mb-3" style="float: left;">
							<img src="assets/img/wpap.jpg">
						</div>
						<div class="title-order" style="float: left;">
							<h5 class="b-600">WPAP Wajah</h5>
						</div>
					</div>
					<div class="clearfix"></div>
					
				</div>
				<div class="col-12 col-sm-6 col-md-6 col-lg-6 label-price">
					<h6 class="grey">Harga</h6>
					<h5 class="b-600 mb-3">Rp 150.000</h5>
					<a href="keranjang-belanja.php" class="bg-blue py-1 px-3 white">Kembali</a>
				</div>
			</div>
			<div class="row my-4">
				<div class="col-5 col-sm-3 col-md-3 col-lg-2 f-12">
					<h6 class="grey">ID Order</h6>
					<h6 class="grey">Tanggal Pesan</h6>
					<h6 class="grey">Pelanggan</h6>
					<h6 class="grey">Ilustrasi</h6>
					<h6 class="grey">Jenis</h6>
				</div>
				<div class="col-7 col-sm-9 col-md-9 col-lg-10 f-12">
					<h6 class="b-600">: PI/09-2018-0988</h6>
					<h6 class="b-600">: 20 Desember 2018</h6>
					<h6 class="b-600">: Ferguso</h6>
					<h6 class="b-600">: WPAP</h6>
					<h6 class="b-600">: Digital</h6>
				</div>
			</div>
			<div>
				<h6 class="b-600">Catatan dari pembeli</h6>
				<p>
					Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.
				</p>
			</div>
			<div class="my-4">
				<h6 class="b-600">Yang Harus kamu kirimkan</h6>
				<div class="row" >
					<div class="col-6 col-sm-3 col-md-3 col-lg-2 ">
						<h6 class="grey">Jumlah Subjek</h6>
						<h6 class="grey">Warna</h6>
						<h6 class="grey">Proporsi</h6>
						<h6 class="grey">Jenis FIle</h6>
					</div>
					<div class="col-6 col-sm-9 col-md-9 col-lg-10">
						<h6>: 1</h6>
						<h6>: Full Color</h6>
						<h6>: Close-up</h6>
						<h6>: JPG</h6>
					</div>
				</div>
			</div>
			<div>
				<h6 class="b-600">Fitur Tambahan</h6>
				<div class="row" >
					<div class="col-6 col-sm-3 col-md-3 col-lg-2">
						<h6 class="grey">Tambah Subjek</h6>
					</div>
					<div class="col-6 col-sm-9 col-md-9 col-lg-10">
						<h6>: 1</h6>
					</div>
				</div>
			</div>
			<div class="attachment-img mt-4">
				<h6 class="b-600">Lampiran</h6>
				<img src="assets/img/profil3.jpg" >
				<img src="assets/img/profil4.jpg" >
				<br>
				<small>Silahkan download foto diatas</small>
			</div>
		</div>

	</div>
</div>

<?php
include "footer-dashboard.php";
?>
<?php
include "footer.php";
?>