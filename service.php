<?php
include "header.php";
?>
<?php
include "header2.php";
?>

<div id="service">
	<div class="container">
		<div class="title mb-5" align="center">
			<h2>Service</h2>
		</div>
		<div class="row">
			<div class="col-12 col-sm-12 col-md-6 col-lg-6">
				<div class="list-service">
					<a href="">
						<div class="row">
							<div class="col-8 col-sm-8 col-md-8 col-lg-8 col-xl-9">
								<h5>WPAP</h5>
								<p>Lorem ipsum dolor sit amet, consectetur a</p>
							</div>
							<div class="col-4 col-sm-4 col-md-4 col-lg-4 col-xl-3" align="right">
								<img src="assets/img/b1.jpg" class="img-service">
							</div>
						</div>
					</a>					
				</div>
			</div>
			<div class="col-12 col-sm-12 col-md-6 col-lg-6">
				<div class="list-service">
					<a href="">
						<div class="row">
							<div class="col-8 col-sm-8 col-md-8 col-lg-8 col-xl-9">
								<h5>Vektor</h5>
								<p>Lorem ipsum dolor sit amet, consectetur a</p>
							</div>
							<div class="col-4 col-sm-4 col-md-4 col-lg-4 col-xl-3" align="right">
								<img src="assets/img/b1.jpg" class="img-service">
							</div>
						</div>
					</a>					
				</div>
			</div>

			<div class="col-12 col-sm-12 col-md-6 col-lg-6">
				<div class="list-service">
					<a href="">
						<div class="row">
							<div class="col-8 col-sm-8 col-md-8 col-lg-8 col-xl-9">
								<h5>Karikatur</h5>
								<p>Lorem ipsum dolor sit amet, consectetur a</p>
							</div>
							<div class="col-4 col-sm-4 col-md-4 col-lg-4 col-xl-3" align="right">
								<img src="assets/img/b1.jpg" class="img-service">
							</div>
						</div>
					</a>					
				</div>
			</div>
			<div class="col-12 col-sm-12 col-md-6 col-lg-6">
				<div class="list-service">
					<a href="">
						<div class="row">
							<div class="col-8 col-sm-8 col-md-8 col-lg-8 col-xl-9">
								<h5>Vexel</h5>
								<p>Lorem ipsum dolor sit amet, consectetur a</p>
							</div>
							<div class="col-4 col-sm-4 col-md-4 col-lg-4 col-xl-3" align="right">
								<img src="assets/img/b1.jpg" class="img-service">
							</div>
						</div>
					</a>					
				</div>
			</div>

			<div class="col-12 col-sm-12 col-md-6 col-lg-6">
				<div class="list-service">
					<a href="">
						<div class="row">
							<div class="col-8 col-sm-8 col-md-8 col-lg-8 col-xl-9">
								<h5>Siluet</h5>
								<p>Lorem ipsum dolor sit amet, consectetur a</p>
							</div>
							<div class="col-4 col-sm-4 col-md-4 col-lg-4 col-xl-3" align="right">
								<img src="assets/img/b1.jpg" class="img-service">
							</div>
						</div>
					</a>					
				</div>
			</div>
			<div class="col-12 col-sm-12 col-md-6 col-lg-6">
				<div class="list-service">
					<a href="">
						<div class="row">
							<div class="col-8 col-sm-8 col-md-8 col-lg-8 col-xl-9">
								<h5>Kartun</h5>
								<p>Lorem ipsum dolor sit amet, consectetur a</p>
							</div>
							<div class="col-4 col-sm-4 col-md-4 col-lg-4 col-xl-3" align="right">
								<img src="assets/img/b1.jpg" class="img-service">
							</div>
						</div>
					</a>					
				</div>
			</div>

			<div class="col-12 col-sm-12 col-md-6 col-lg-6">
				<div class="list-service">
					<a href="">
						<div class="row">
							<div class="col-8 col-sm-8 col-md-8 col-lg-8 col-xl-9">
								<h5>Scribble</h5>
								<p>Lorem ipsum dolor sit amet, consectetur a</p>
							</div>
							<div class="col-4 col-sm-4 col-md-4 col-lg-4 col-xl-3" align="right">
								<img src="assets/img/b1.jpg" class="img-service">
							</div>
						</div>
					</a>					
				</div>
			</div>
			<div class="col-12 col-sm-12 col-md-6 col-lg-6">
				<div class="list-service">
					<a href="">
						<div class="row">
							<div class="col-8 col-sm-8 col-md-8 col-lg-8 col-xl-9">
								<h5>Doodle</h5>
								<p>Lorem ipsum dolor sit amet, consectetur a</p>
							</div>
							<div class="col-4 col-sm-4 col-md-4 col-lg-4 col-xl-3" align="right">
								<img src="assets/img/b1.jpg" class="img-service">
							</div>
						</div>
					</a>					
				</div>
			</div>

			<div class="col-12 col-sm-12 col-md-6 col-lg-6">
				<div class="list-service">
					<a href="">
						<div class="row">
							<div class="col-8 col-sm-8 col-md-8 col-lg-8 col-xl-9">
								<h5>Smudge</h5>
								<p>Lorem ipsum dolor sit amet, consectetur a</p>
							</div>
							<div class="col-4 col-sm-4 col-md-4 col-lg-4 col-xl-3" align="right">
								<img src="assets/img/b1.jpg" class="img-service">
							</div>
						</div>
					</a>					
				</div>
			</div>
			<div class="col-12 col-sm-12 col-md-6 col-lg-6">
				<div class="list-service">
					<a href="">
						<div class="row">
							<div class="col-8 col-sm-8 col-md-8 col-lg-8 col-xl-9">
								<h5>Water Color</h5>
								<p>Lorem ipsum dolor sit amet, consectetur a</p>
							</div>
							<div class="col-4 col-sm-4 col-md-4 col-lg-4 col-xl-3" align="right">
								<img src="assets/img/b1.jpg" class="img-service">
							</div>
						</div>
					</a>					
				</div>
			</div>

			<div class="col-12 col-sm-12 col-md-6 col-lg-6">
				<div class="list-service">
					<a href="">
						<div class="row">
							<div class="col-8 col-sm-8 col-md-8 col-lg-8 col-xl-9">
								<h5>Line Art</h5>
								<p>Lorem ipsum dolor sit amet, consectetur a</p>
							</div>
							<div class="col-4 col-sm-4 col-md-4 col-lg-4 col-xl-3" align="right">
								<img src="assets/img/b1.jpg" class="img-service">
							</div>
						</div>
					</a>					
				</div>
			</div>
			<div class="col-12 col-sm-12 col-md-6 col-lg-6">
				<div class="list-service">
					<a href="">
						<div class="row">
							<div class="col-8 col-sm-8 col-md-8 col-lg-8 col-xl-9">
								<h5>POP Art</h5>
								<p>Lorem ipsum dolor sit amet, consectetur a</p>
							</div>
							<div class="col-4 col-sm-4 col-md-4 col-lg-4 col-xl-3" align="right">
								<img src="assets/img/b1.jpg" class="img-service">
							</div>
						</div>
					</a>					
				</div>
			</div>

		</div>
	</div>
</div>


<?php
include "footer2.php";
?>
<?php
include "footer.php";
?>