<?php
include "header.php";
?>
<?php
include "header-dashboard.php";
?>

<div id="dashboard" class="dashboard bg-grey">
	<div class="container">
		<div class="row">
			<div class="col-12 col-sm-12 col-md-12 col-lg-5 col-xl-4">
				<div class="bg-white list-message py-2">
					<div class="px-2">
						<h5>Kotak Pesan</h5>
					</div>
					<div class="scrollbar" id="style-2">
						<div class="force-overflow">
							
							<div class="message active px-2 border-bottom ">
								<h6>Admin Pickpict</h6>
								<p>Terima kasih telah bergabung dengan pickpict</p>
								<small>20 Desember 2018, 18.00</small>
							</div>
							<div class="message py-2 px-2 border-bottom">
								<h6>John dae</h6>
								<p>Saya minta direvisi dong, tolong tambahain jadi full bad..</p>
								<small>20 Desember 2018, 18.00</small>
							</div>
							<div class="message py-2 px-2 border-bottom">
								<h6>Tony Stark</h6>
								<p>Saya minta direvisi dong</p>
								<small>20 Desember 2018, 18.00</small>
							</div>


						</div>
					</div>
				</div>
			</div>
			<div class="col-12 col-sm-12 col-md-12 col-lg-7 col-xl-8">
				<div class="bg-white">
					<div class="sender">
						<div class="row py-2 px-2">
							<div class="col-10 col-sm-10 col-md-10 col-lg-10">
								<img src="assets/img/profil3.jpg"> Admin Pickpict
							</div>
							<div class="col-2 col-sm-2 col-md-2 col-lg-2" align="right">

								<div class="btn-group dropleft">
									<a class="btn"id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
										<i class="fas fa-ellipsis-v"></i>
									</a>
									<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
										<a class="dropdown-item" href="creator.php">Lihat profil</a>
										<a class="dropdown-item" href="#">Hapus pesan</a>
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="send-box ">

						<div class="scrollbar" id="style-2">
							<div class="force-overflow">

								<div class="your-message" >
									<h6>Terima kasih telah bergabung dengan pickpict</h6>
									<small>20 Desember 2018, 18.00</small>
								</div>

								<div class="message2" align="right">
									<h6>sama-sama</h6>
									<small>20 Desember 2018, 18.30</small>
								</div>



							</div>
						</div>

					</div>

					<div class="type-message">
						<div class="container">
							<div class="row py-3">
								<div class="col-12 col-sm-10 col-md-10 col-lg-10">
									<textarea class="form-control" id="" rows="1" placeholder="Ketik pesan kamu ....."></textarea>
								</div>
								<div class="col-2 col-sm-2 col-md-2 col-lg-2">
									<div class="btn-blue">
									<button type="submit" class="btn bg-biru btn-sm btn-block">kirim</button>										
									</div>
								</div>
							</div>
						</div>
					</div>
					
				</div>
			</div>
		</div>
	</div>
</div>




<?php
include "footer-dashboard.php";
?>
<?php
include "footer.php";
?>