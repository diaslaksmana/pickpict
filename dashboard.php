<?php
include "header.php";
?>
<?php
include "header-dashboard.php";
?>

<div id="dashboard" class="dashboard bg-grey">
	<div class="container">
		<div class="row">
			<div class="col-12 col-sm-12 col-md-4 col-lg-3 col-xl-3">
				<div class="profil-picture bg-white mb-4" align="center">
					<div class="setting mb-3">
						<i class="fas fa-cog"></i>
					</div>
					<div class="your-name mb-4" align="left">
						<h5>Halo,</h5>
						<h6>John Dae</h6>
					</div>
					
					<img src="assets/img/profil3.jpg">
				</div>

				<div class="bg-white b-r-5 pb-3" align="center">
					<h5 class="pt-3 pt-2">Rating</h5>
					<div>
						<i class="fas fa-star"></i>
						<i class="fas fa-star"></i>
						<i class="fas fa-star"></i>
						<i class="fas fa-star"></i>
						<i class="fas fa-star"></i>
					</div>
					<small class="grey mb-3"><i>(10 Ulasan)</i></small>
				</div>

				<div class="saldo-now b-r-5 bg-white py-3 mt-4 mb-5" align="center">
					<h5>Saldo Tersedia</h5>
					<h4 class="b-600">Rp 1.500.000</h4>
					<a href="">(cairkan)</a>
				</div>
			</div>
			<div class="col-12 col-sm-12 col-md-8 col-lg-9 col-xl-9">
				<div class="">
					<h6 class="b-600">(3) Pesanan sedang aktif</h6>

					<div class="order-active b-r-5 bg-white">
						<div class="container">
							<div class="row scroll">
								<div class="col-1 col-sm-1 col-md-1 col-lg-1 col-xl-1 ">
									<div class="img2">
										<img src="assets/img/wpap.jpg">
									</div>							
								</div>
								<div class="col-2 col-sm-2 col-md-2 col-lg-2 col-xl-2">
									<div class="pt-3">
										<h6>Ilustrasi WPAP</h6>
									</div>
								</div>
								<div class="col-2 col-sm-2 col-md-2 col-lg-2 col-xl-2 list2">
									<div  align="center">
										<small>Ilustrasi</small>
										<h6>WPAP</h6>
									</div>							
								</div>
								<div class="col-1 col-sm-1 col-md-1 col-lg-1 col-xl-1 list2">
									<div  align="center">
										<small>Tipe</small>
										<h6>Digital</h6>
									</div>							
								</div>
								<div class="col-2 col-sm-2 col-md-2 col-lg-2 col-xl-2 list2">
									<div align="center">
										<small>Deadline</small>
										<h6>30 Desember 2018</h6>
									</div>							
								</div>
								<div class="col-2 col-sm-2 col-md-2 col-lg-2 col-xl-2 list2">
									<div  align="center">
										<small>Harga</small>
										<h6>Rp.100.000</h6>
									</div>							
								</div>
								<div class="col-2 col-sm-2 col-md-2 col-lg-2 col-xl-2 list2">
									<div class="btn-detail-order pt-3" align="center">
										<a class="bg-yellow" href="rincian-pesanan.php">
											Rincian
										</a>
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="order-active b-r-5 bg-white">
						<div class="container">
							<div class="row scroll">
								<div class="col-1 col-sm-1 col-md-1 col-lg-1 col-xl-1 ">
									<div class="img2">
										<img src="assets/img/wpap.jpg">
									</div>							
								</div>
								<div class="col-2 col-sm-2 col-md-2 col-lg-2 col-xl-2">
									<div class="pt-3">
										<h6>Ilustrasi WPAP</h6>
									</div>
								</div>
								<div class="col-2 col-sm-2 col-md-2 col-lg-2 col-xl-2 list2">
									<div  align="center">
										<small>Ilustrasi</small>
										<h6>WPAP</h6>
									</div>							
								</div>
								<div class="col-1 col-sm-1 col-md-1 col-lg-1 col-xl-1 list2">
									<div  align="center">
										<small>Tipe</small>
										<h6>Digital</h6>
									</div>							
								</div>
								<div class="col-2 col-sm-2 col-md-2 col-lg-2 col-xl-2 list2">
									<div align="center">
										<small>Deadline</small>
										<h6>30 Desember 2018</h6>
									</div>							
								</div>
								<div class="col-2 col-sm-2 col-md-2 col-lg-2 col-xl-2 list2">
									<div  align="center">
										<small>Harga</small>
										<h6>Rp.100.000</h6>
									</div>							
								</div>
								<div class="col-2 col-sm-2 col-md-2 col-lg-2 col-xl-2 list2">
									<div class="btn-detail-order pt-3" align="center">
										<a class="bg-yellow" href="rincian-pesanan.php">
											Rincian
										</a>
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="order-active b-r-5 bg-white">
						<div class="container">
							<div class="row scroll">
								<div class="col-1 col-sm-1 col-md-1 col-lg-1 col-xl-1 ">
									<div class="img2">
										<img src="assets/img/wpap.jpg">
									</div>							
								</div>
								<div class="col-2 col-sm-2 col-md-2 col-lg-2 col-xl-2">
									<div class="pt-3">
										<h6>Ilustrasi WPAP</h6>
									</div>
								</div>
								<div class="col-2 col-sm-2 col-md-2 col-lg-2 col-xl-2 list2">
									<div  align="center">
										<small>Ilustrasi</small>
										<h6>WPAP</h6>
									</div>							
								</div>
								<div class="col-1 col-sm-1 col-md-1 col-lg-1 col-xl-1 list2">
									<div  align="center">
										<small>Tipe</small>
										<h6>Digital</h6>
									</div>							
								</div>
								<div class="col-2 col-sm-2 col-md-2 col-lg-2 col-xl-2 list2">
									<div align="center">
										<small>Deadline</small>
										<h6>30 Desember 2018</h6>
									</div>							
								</div>
								<div class="col-2 col-sm-2 col-md-2 col-lg-2 col-xl-2 list2">
									<div  align="center">
										<small>Harga</small>
										<h6>Rp.100.000</h6>
									</div>							
								</div>
								<div class="col-2 col-sm-2 col-md-2 col-lg-2 col-xl-2 list2">
									<div class="btn-detail-order pt-3" align="center">
										<a class="bg-yellow" href="rincian-pesanan.php">
											Rincian
										</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="mt-4">
					<h6 class="b-600">(3) Pembelian aktif</h6>

					<div class="buying-active b-r-5 bg-white">
						<div class="container">
							<div class="row scroll">
								<div class="col-3 col-sm-3 col-md-3 col-lg-3 col-xl-3">
									<div>
										<small>ID Transaksi</small>
										<h6>PI-0887987-080923</h6>
									</div>
								</div>
								<div class="col-2 col-sm-2 col-md-2 col-lg-2 col-xl-2 list2">
									<div  align="center">
										<small>Total</small>
										<h6>Rp 150.000</h6>
									</div>							
								</div>
								<div class="col-3 col-sm-3 col-md-3 col-lg-3 col-xl-3 list2">
									<div align="center">
										<small>Batas Pembayaran</small>
										<h6>30 Desember 2018</h6>
									</div>							
								</div>
								<div class="col-2 col-sm-2 col-md-2 col-lg-2 col-xl-2 list2">
									<div  align="center">
										<small>Status</small>
										<h6>Baru</h6>
									</div>							
								</div>
								<div class="col-2 col-sm-2 col-md-2 col-lg-2 col-xl-2 list2">
									<div class="btn-detail-order pt-2" align="center">
										<a class="bg-pink" href="detail-status-pembayaran.php">
											Rincian
										</a>
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="buying-active b-r-5 bg-white">
						<div class="container">
							<div class="row scroll">
								<div class="col-3 col-sm-3 col-md-3 col-lg-3 col-xl-3">
									<div>
										<small>ID Transaksi</small>
										<h6>PI-0887987-080923</h6>
									</div>
								</div>
								<div class="col-2 col-sm-2 col-md-2 col-lg-2 col-xl-2 list2">
									<div  align="center">
										<small>Total</small>
										<h6>Rp 150.000</h6>
									</div>							
								</div>
								<div class="col-3 col-sm-3 col-md-3 col-lg-3 col-xl-3 list2">
									<div align="center">
										<small>Batas Pembayaran</small>
										<h6>30 Desember 2018</h6>
									</div>							
								</div>
								<div class="col-2 col-sm-2 col-md-2 col-lg-2 col-xl-2 list2">
									<div  align="center">
										<small>Status</small>
										<h6>Baru</h6>
									</div>							
								</div>
								<div class="col-2 col-sm-2 col-md-2 col-lg-2 col-xl-2 list2">
									<div class="btn-detail-order pt-2" align="center">
										<a class="bg-pink" href="detail-status-pembayaran.php">
											Rincian
										</a>
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="buying-active b-r-5 bg-white">
						<div class="container">
							<div class="row scroll">
								<div class="col-3 col-sm-3 col-md-3 col-lg-3 col-xl-3">
									<div>
										<small>ID Transaksi</small>
										<h6>PI-0887987-080923</h6>
									</div>
								</div>
								<div class="col-2 col-sm-2 col-md-2 col-lg-2 col-xl-2 list2">
									<div  align="center">
										<small>Total</small>
										<h6>Rp 150.000</h6>
									</div>							
								</div>
								<div class="col-3 col-sm-3 col-md-3 col-lg-3 col-xl-3 list2">
									<div align="center">
										<small>Batas Pembayaran</small>
										<h6>30 Desember 2018</h6>
									</div>							
								</div>
								<div class="col-2 col-sm-2 col-md-2 col-lg-2 col-xl-2 list2">
									<div  align="center">
										<small>Status</small>
										<h6>Baru</h6>
									</div>							
								</div>
								<div class="col-2 col-sm-2 col-md-2 col-lg-2 col-xl-2 list2">
									<div class="btn-detail-order pt-2" align="center">
										<a class="bg-pink" href="detail-status-pembayaran.php">
											Rincian
										</a>
									</div>
								</div>
							</div>
						</div>
					</div>

				</div>

				<div class="mt-4">
					<h6 class="b-600">Pendapatan</h6>
					<div class="row">
						<div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-3">
							<div class="bg-white b-r-5 py-3 mb-3" align="center">
								<small class="grey">Pendapatan Bersih</small>
								<h5 class="b-600 pt-2">Rp 2.500.000</h5>
							</div>
						</div>
						<div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-3">
							<div class="bg-white b-r-5 py-3 mb-3" align="center">
								<small class="grey">Pendapatan Telah dicairkan</small>
								<h5 class="b-600 pt-2">Rp 3.500.000</h5>
							</div>
						</div>
						<div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-3">
							<div class="bg-white b-r-5 py-3 mb-3" align="center">
								<small class="grey">Pesanan Ilustrasi Aktif</small>
								<h5 class="b-600 pt-2">Rp 500.000</h5>
							</div>
						</div>
						<div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-3">
							<div class="bg-white b-r-5 py-3 mb-3" align="center">
								<small class="grey">Pesanan Koleksi Aktif</small>
								<h5 class="b-600 pt-2">Rp 1.000.000</h5>
							</div>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>
</div>

<?php
include "footer-dashboard.php";
?>
<?php
include "footer.php";
?>