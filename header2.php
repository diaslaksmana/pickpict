<div  id="header" class="header">
	<div class="container">
		<div class="row pt-3">
			<div class="col-3 col-sm-3 col-md-2 col-lg-2">
				<div class="brand-logo">
					<a href="index.php">
						<h4 style="color: black">PICKPICT</h4>
					</a>					
				</div>
			</div>
			<div class="col-1 col-sm-5 col-md-4 col-lg-5 col-search">
				<div class="search">
					<div class="input-group">
						<input type="text" class="form-control " id="" aria-describedby="" placeholder="Saya ingin mencari ">
						<span class="input-group-addon" id="sizing-addon2">
							<i class="fa fa-search"></i>
						</span>
					</div>	
				</div>			
			</div>
			<div class="col-9 col-sm-9 col-md-6 col-lg-5">
				<div class="menu-desktop">
					<ul>
						<li class="hidden-menu-mobile">
							<a href="service.php">
								Service
							</a>
						</li>
						<li class="hidden-menu-mobile">
							<a href="koleksi.php">
								Koleksi
							</a>
						</li>
						<li>
							<a href="favorit.php">
								<i class="fas fa-heart"></i>
							</a>
						</li>
						<li>
							<a href="">
								<i class="fas fa-shopping-bag"></i>
							</a>
						</li>
						<li class="hidden-menu-mobile">
							<div class="btn-login">
								<a href="" data-toggle="modal" data-target="#loginModal">Login</a>
							</div>							
						</li>
						<li>
							<div  class="icon-menu-mobile">
								<i class="fas fa-stream" onclick="sideMenu()"></i>
							</div>
							
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>	


	<!-- sidemenu -->

	<div id="menuMobile" class="overlay">
		<a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
		<div class="overlay-content">
			<div class="search mr-4">
				<div class="input-group">
					<input type="text" class="form-control " id="" aria-describedby="" placeholder="Saya ingin mencari ">
					<span class="input-group-addon" id="sizing-addon2">
						<i class="fa fa-search"></i>
					</span>
				</div>	
			</div>	
			<a href="service.php">Service</a>
			<a href="koleksi.php">Koleksi</a>
			<a href="" data-toggle="modal" data-target="#loginModal">Login</a>
		</div>
	</div>
</div>

<!-- Modal Login -->
<div class="modal fade" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Login Pickpict</h5>
			</div>
			<div class="modal-body ">
				<form class="">
					<center>                
						<div class="floating-form mt-3">
							<div class="floating-label">      
								<input class="floating-input" type="text" placeholder=" ">
								<span class="highlight"></span>
								<label><i class="fas fa-user"></i> Username atau E-Mail</label>
							</div>

							<div class="floating-label">      
								<input class="floating-input" type="password" placeholder=" ">
								<span class="highlight"></span>
								<label><i class="fas fa-lock"></i> Password</label>
							</div> 
						</div>
						<div class="forget-password">
							<p>Lupa Password ? <a href="#"> Reset Password </a></p>
						</div>
						<div class="btn-login mb-3">
							<button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Batal</button>
							<button type="submit" class="btn btn-blue btn-sm">Masuk</button>
						</div>
						<div class="login-with">
							<h6>Login With</h6>
							<a href=""><img src="assets/img/icon/google.png">Login dengan Google</a>
							<a href=""><img src="assets/img/icon/fb.png">Login dengan Facebook</a>
						</div>
					</center>

				</form>

			</div>
			<div class="register-account" align="center">
				<p>
					Belum Punya Akun ? <a href="register.php"> Silahkan daftar disini</a>
				</p>
			</div>
		</div>
	</div>
</div>