<?php
include "header.php";
?>
<?php
include "header-dashboard.php";
?>

<div id="dashboard" class="dashboard bg-grey">
	<div class="container">
		<div class="row">
			<div class="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-2">
				<div class="nav-pills-blue mb-5">
					<div class="nav flex-column " id="v-pills-tab" role="tablist" aria-orientation="vertical">
						<a class="nav-link active" id="v-pills-pesanan-aktif-tab" data-toggle="pill" href="#v-pills-pesanan-aktif" role="tab" aria-controls="v-pills-pesanan-aktif" aria-selected="true">
							<i class="fas fa-clipboard-list"></i> Pesanan Aktif
						</a>
						<a class="nav-link" id="v-pills-status-pesanan-tab" data-toggle="pill" href="#v-pills-status-pesanan" role="tab" aria-controls="v-pills-status-pesanan" aria-selected="false">
							<i class="fas fa-clipboard-check"></i> Status Pesanan
						</a>
					</div>
				</div>				
			</div>
			<div class="col-12 col-sm-12 col-md-9 col-lg-9 col-xl-10">
				<div class="tab-content" id="v-pills-tabContent">
					<div class="tab-pane fade show active" id="v-pills-pesanan-aktif" role="tabpanel" aria-labelledby="v-pills-pesanan-aktif-tab">

						<!--Pesanan Aktif-->
						<h6 class="b-600">(3) Pesanan sedang aktif</h6>

						<div class="order-active b-r-5 bg-white">
							<div class="container">
								<div class="row scroll">
									<div class="col-1 col-sm-1 col-md-1 col-lg-1 col-xl-1 ">
										<div class="img2">
											<img src="assets/img/wpap.jpg">
										</div>							
									</div>
									<div class="col-2 col-sm-2 col-md-2 col-lg-2 col-xl-2">
										<div class="pt-3">
											<h6>Ilustrasi WPAP</h6>
										</div>
									</div>
									<div class="col-2 col-sm-2 col-md-2 col-lg-2 col-xl-2 list2">
										<div  align="center">
											<small>Ilustrasi</small>
											<h6>WPAP</h6>
										</div>							
									</div>
									<div class="col-1 col-sm-1 col-md-1 col-lg-1 col-xl-1 list2">
										<div  align="center">
											<small>Tipe</small>
											<h6>Digital</h6>
										</div>							
									</div>
									<div class="col-2 col-sm-2 col-md-2 col-lg-2 col-xl-2 list2">
										<div align="center">
											<small>Deadline</small>
											<h6>30 Desember 2018</h6>
										</div>							
									</div>
									<div class="col-2 col-sm-2 col-md-2 col-lg-2 col-xl-2 list2">
										<div  align="center">
											<small>Harga</small>
											<h6>Rp.100.000</h6>
										</div>							
									</div>
									<div class="col-2 col-sm-2 col-md-2 col-lg-2 col-xl-2 list2">
										<div class="btn-detail-order pt-3" align="center">
											<a class="bg-yellow" href="rincian-pesanan.php">
												Rincian
											</a>
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="order-active b-r-5 bg-white">
							<div class="container">
								<div class="row scroll">
									<div class="col-1 col-sm-1 col-md-1 col-lg-1 col-xl-1 ">
										<div class="img2">
											<img src="assets/img/wpap.jpg">
										</div>							
									</div>
									<div class="col-2 col-sm-2 col-md-2 col-lg-2 col-xl-2">
										<div class="pt-3">
											<h6>Ilustrasi WPAP</h6>
										</div>
									</div>
									<div class="col-2 col-sm-2 col-md-2 col-lg-2 col-xl-2 list2">
										<div  align="center">
											<small>Ilustrasi</small>
											<h6>WPAP</h6>
										</div>							
									</div>
									<div class="col-1 col-sm-1 col-md-1 col-lg-1 col-xl-1 list2">
										<div  align="center">
											<small>Tipe</small>
											<h6>Digital</h6>
										</div>							
									</div>
									<div class="col-2 col-sm-2 col-md-2 col-lg-2 col-xl-2 list2">
										<div align="center">
											<small>Deadline</small>
											<h6>30 Desember 2018</h6>
										</div>							
									</div>
									<div class="col-2 col-sm-2 col-md-2 col-lg-2 col-xl-2 list2">
										<div  align="center">
											<small>Harga</small>
											<h6>Rp.100.000</h6>
										</div>							
									</div>
									<div class="col-2 col-sm-2 col-md-2 col-lg-2 col-xl-2 list2">
										<div class="btn-detail-order pt-3" align="center">
											<a class="bg-yellow" href="rincian_pesanan.php">
												Rincian
											</a>
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="order-active b-r-5 bg-white">
							<div class="container">
								<div class="row scroll">
									<div class="col-1 col-sm-1 col-md-1 col-lg-1 col-xl-1 ">
										<div class="img2">
											<img src="assets/img/wpap.jpg">
										</div>							
									</div>
									<div class="col-2 col-sm-2 col-md-2 col-lg-2 col-xl-2">
										<div class="pt-3">
											<h6>Ilustrasi WPAP</h6>
										</div>
									</div>
									<div class="col-2 col-sm-2 col-md-2 col-lg-2 col-xl-2 list2">
										<div  align="center">
											<small>Ilustrasi</small>
											<h6>WPAP</h6>
										</div>							
									</div>
									<div class="col-1 col-sm-1 col-md-1 col-lg-1 col-xl-1 list2">
										<div  align="center">
											<small>Tipe</small>
											<h6>Digital</h6>
										</div>							
									</div>
									<div class="col-2 col-sm-2 col-md-2 col-lg-2 col-xl-2 list2">
										<div align="center">
											<small>Deadline</small>
											<h6>30 Desember 2018</h6>
										</div>							
									</div>
									<div class="col-2 col-sm-2 col-md-2 col-lg-2 col-xl-2 list2">
										<div  align="center">
											<small>Harga</small>
											<h6>Rp.100.000</h6>
										</div>							
									</div>
									<div class="col-2 col-sm-2 col-md-2 col-lg-2 col-xl-2 list2">
										<div class="btn-detail-order pt-3" align="center">
											<a class="bg-yellow" href="rincian_pesanan.php">
												Rincian
											</a>
										</div>
									</div>
								</div>
							</div>
						</div>

						<small>* jangan lupa lihat tanggal deadline pengerjaannya</small>

					</div>
					<div class="tab-pane fade" id="v-pills-status-pesanan" role="tabpanel" aria-labelledby="v-pills-status-pesanan-tab">

						<div class="bg-white pb-3">
							
							<div class="nav-pills-border-blue">
								<div class="scroll">
									<ul class="nav mb-3" id="pills-tab" role="tablist">
										<li class="nav-item">
											<a class="nav-link active" id="pills-aktif-tab" data-toggle="pill" href="#pills-aktif" role="tab" aria-controls="pills-aktif" aria-selected="true">
												<i class="far fa-clock"></i> Aktif
											</a>
										</li>
										<li class="nav-item">
											<a class="nav-link" id="pills-selesei-tab" data-toggle="pill" href="#pills-selesei" role="tab" aria-controls="pills-selesei" aria-selected="false">
												<i class="far fa-calendar-check"></i> Selesei
											</a>
										</li>
										<li class="nav-item">
											<a class="nav-link" id="pills-terkirim-tab" data-toggle="pill" href="#pills-terkirim" role="tab" aria-controls="pills-terkirim" aria-selected="false">
												<i class="fas fa-truck"></i> Terkirim
											</a>
										</li>
										<li class="nav-item">
											<a class="nav-link" id="pills-dibatalkan-tab" data-toggle="pill" href="#pills-dibatalkan" role="tab" aria-controls="pills-dibatalkan" aria-selected="false">
												<i class="fas fa-times-circle"></i> Dibatalkan
											</a>
										</li>
									</ul>
								</div>								
							</div>


							<div class="tab-content" id="pills-tabContent">
								<div class="tab-pane fade show active" id="pills-aktif" role="tabpanel" aria-labelledby="pills-aktif-tab">

									<!--Aktif-->
									<div class="table-responsive">
										<table class="table table-striped font-table">
											<thead>
												<tr>
													<th scope="col">ID Order</th>
													<th scope="col">Tanggal Pesan</th>
													<th scope="col">Pemesan</th>
													<th scope="col">Ilustrasi</th>
													<th scope="col">jenis</th>
													<th scope="col">Batas Pengiriman</th>
													<th scope="col">Status</th>
												</tr>
											</thead>
											<tbody>
												<tr>
													<th scope="row">PI-2018-09-1</th>
													<td>20 Desember 2018</td>
													<td>Dias</td>
													<td>WPAP</td>
													<td>Digital</td>
													<td>23 Desember 2018</td>
													<td class="blue">Dalam Pengerjaan</td>
												</tr>
												<tr>
													<th scope="row">PI-2018-09-2</th>
													<td>20 Desember 2018</td>
													<td>Dias</td>
													<td>WPAP</td>
													<td>Cetak Produk</td>
													<td>27 Desember 2018</td>
													<td class="blue">Dalam Pengerjaan</td>
												</tr>
											</tbody>
										</table>
									</div>		

									<div class="page-number">
										<nav aria-label="Page navigation ">
											<ul class="pagination justify-content-center">
												<li class="disabled">
													<a class="" href="#" tabindex="-1" aria-disabled="true">
														<i class="fas fa-chevron-circle-left"></i>
													</a>
												</li>
												<li class="active"><a class="" href="#">1</a></li>
												<li class=""><a class="" href="#">2</a></li>
												<li class=""><a class="" href="#">3</a></li>
												<li class=""><a class="" href="#">4</a></li>
												<li class=""><a class="" href="#">5</a></li>
												<li class="">
													<a class="" href="#">
														<i class="fas fa-chevron-circle-right"></i>
													</a>
												</li>
											</ul>
										</nav>	
									</div>


								</div>
								<div class="tab-pane fade" id="pills-selesei" role="tabpanel" aria-labelledby="pills-selesei-tab">
									
									<!--Selesei-->
									<div class="table-responsive">
										<table class="table table-striped font-table">
											<thead>
												<tr>
													<th scope="col">ID Order</th>
													<th scope="col">Pemesan</th>
													<th scope="col">Ilustrasi</th>
													<th scope="col">jenis</th>
													<th scope="col">Dikirim Tanggal</th>
													<th scope="col">Status</th>
												</tr>
											</thead>
											<tbody>
												<tr>
													<th scope="row">PI-2018-09-1</th>
													<td>Dias</td>
													<td>WPAP</td>
													<td>Digital</td>
													<td>23 Desember 2018</td>
													<td class="blue">Selesei</td>
												</tr>
												<tr>
													<th scope="row">PI-2018-09-2</th>
													<td>Dias</td>
													<td>WPAP</td>
													<td>Cetak Produk</td>
													<td>27 Desember 2018</td>
													<td class="blue">Selesei</td>
												</tr>
											</tbody>
										</table>
									</div>	

									<div class="page-number">
										<nav aria-label="Page navigation ">
											<ul class="pagination justify-content-center">
												<li class="disabled">
													<a class="" href="#" tabindex="-1" aria-disabled="true">
														<i class="fas fa-chevron-circle-left"></i>
													</a>
												</li>
												<li class="active"><a class="" href="#">1</a></li>
												<li class=""><a class="" href="#">2</a></li>
												<li class=""><a class="" href="#">3</a></li>
												<li class=""><a class="" href="#">4</a></li>
												<li class=""><a class="" href="#">5</a></li>
												<li class="">
													<a class="" href="#">
														<i class="fas fa-chevron-circle-right"></i>
													</a>
												</li>
											</ul>
										</nav>	
									</div>


								</div>
								<div class="tab-pane fade" id="pills-terkirim" role="tabpanel" aria-labelledby="pills-terkirim-tab">

									<!--Terkirim-->
									<div class="table-responsive">
										<table class="table table-striped font-table">
											<thead>
												<tr>
													<th scope="col">ID Order</th>
													<th scope="col">Pemesan</th>
													<th scope="col">Ilustrasi</th>
													<th scope="col">jenis</th>
													<th scope="col">Tanggal Pengiriman</th>
													<th scope="col">Status</th>
												</tr>
											</thead>
											<tbody>
												<tr>
													<th scope="row">PI-2018-09-1</th>
													<td>Dias</td>
													<td>WPAP</td>
													<td>Digital</td>
													<td>23 Desember 2018</td>
													<td class="blue">
														Sampai <i class="fas fa-check"></i>
													</td>
												</tr>
												<tr>
													<th scope="row">PI-2018-09-2</th>
													<td>Dias</td>
													<td>WPAP</td>
													<td>Cetak Produk</td>
													<td>27 Desember 2018</td>
													<td class="blue">
														Sampai <i class="fas fa-check"></i>
													</td>
												</tr>
												<tr>
													<th scope="row">PI-2018-09-2</th>
													<td>Dias</td>
													<td>WPAP</td>
													<td>Cetak Produk</td>
													<td>27 Desember 2018</td>
													<td class="blue">
														Sampai <i class="fas fa-check"></i>
													</td>
												</tr>
												<tr>
													<th scope="row">PI-2018-09-2</th>
													<td>Dias</td>
													<td>WPAP</td>
													<td>Cetak Produk</td>
													<td>27 Desember 2018</td>
													<td class="blue">
														Sampai <i class="fas fa-check"></i>
													</td>
												</tr>
												<tr>
													<th scope="row">PI-2018-09-2</th>
													<td>Dias</td>
													<td>WPAP</td>
													<td>Cetak Produk</td>
													<td>27 Desember 2018</td>
													<td class="blue">
														Sampai <i class="fas fa-check"></i>
													</td>
												</tr>
											</tbody>
										</table>
									</div>

									<div class="page-number">
										<nav aria-label="Page navigation ">
											<ul class="pagination justify-content-center">
												<li class="disabled">
													<a class="" href="#" tabindex="-1" aria-disabled="true">
														<i class="fas fa-chevron-circle-left"></i>
													</a>
												</li>
												<li class="active"><a class="" href="#">1</a></li>
												<li class=""><a class="" href="#">2</a></li>
												<li class=""><a class="" href="#">3</a></li>
												<li class=""><a class="" href="#">4</a></li>
												<li class=""><a class="" href="#">5</a></li>
												<li class="">
													<a class="" href="#">
														<i class="fas fa-chevron-circle-right"></i>
													</a>
												</li>
											</ul>
										</nav>	
									</div>


								</div>
								<div class="tab-pane fade" id="pills-dibatalkan" role="tabpanel" aria-labelledby="pills-dibatalkan-tab">

									<!--Dibatalkan-->
									<div class="table-responsive">
										<table class="table table-striped font-table">
											<thead>
												<tr>
													<th scope="col">ID Order</th>
													<th scope="col">Pemesan</th>
													<th scope="col">Ilustrasi</th>
													<th scope="col">jenis</th>
													<th scope="col">Alasan Dibatalkan</th>
												</tr>
											</thead>
											<tbody>
												<tr>
													<th scope="row">PI-2018-09-1</th>
													<td>Dias</td>
													<td>WPAP</td>
													<td>Digital</td>
													<td class="status_pesanan_anda">
														Banyak pesanan
													</td>
												</tr>
												<tr>
													<th scope="row">PI-2018-09-2</th>
													<td>Dias</td>
													<td>WPAP</td>
													<td>Cetak Produk</td>
													<td class="status_pesanan_anda">
														Sedang liburan
													</td>
												</tr>
											</tbody>
										</table>
									</div>	

									<div class="page-number">
										<nav aria-label="Page navigation ">
											<ul class="pagination justify-content-center">
												<li class="disabled">
													<a class="" href="#" tabindex="-1" aria-disabled="true">
														<i class="fas fa-chevron-circle-left"></i>
													</a>
												</li>
												<li class="active"><a class="" href="#">1</a></li>
												<li class=""><a class="" href="#">2</a></li>
												<li class=""><a class="" href="#">3</a></li>
												<li class=""><a class="" href="#">4</a></li>
												<li class=""><a class="" href="#">5</a></li>
												<li class="">
													<a class="" href="#">
														<i class="fas fa-chevron-circle-right"></i>
													</a>
												</li>
											</ul>
										</nav>	
									</div>


								</div>
							</div>

						</div>						

					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<?php
include "footer-dashboard.php";
?>
<?php
include "footer.php";
?>