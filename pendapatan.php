<?php
include "header.php";
?>
<?php
include "header-dashboard.php";
?>

<div id="dashboard" class="dashboard bg-grey">
	<div class="container">
		<h5>Pendapatan</h5>
		<div class="row">
			<div class="col-12 col-sm-12 col-md-4 col-lg-3 col-xl-3">
				<div class="nav-pills-blue mb-5">
					<div class="nav flex-column " id="v-pills-tab" role="tablist" aria-orientation="vertical">
						<a class="nav-link active" id="v-pills-ilustrasi-tab" data-toggle="pill" href="#v-pills-ilustrasi" role="tab" aria-controls="v-pills-ilustrasi" aria-selected="true">
							<i class="fas fa-pencil-alt"></i> Ilustrasi
						</a>
						<a class="nav-link" id="v-pills-koleksi-tab" data-toggle="pill" href="#v-pills-koleksi" role="tab" aria-controls="v-pills-koleksi" aria-selected="false">
							<i class="fas fa-gift"></i> Koleksi
						</a>
						<a class="nav-link" id="v-pills-riwayat-tab" data-toggle="pill" href="#v-pills-riwayat" role="tab" aria-controls="v-pills-riwayat" aria-selected="false">
							<i class="fas fa-history"></i> Riwayat
						</a>
					</div>
				</div>				
			</div>
			<div class="col-12 col-sm-12 col-md-8 col-lg-9 col-xl-9">
				<div class="tab-content" id="v-pills-tabContent">
					<div class="tab-pane fade show active" id="v-pills-ilustrasi" role="tabpanel" aria-labelledby="v-pills-ilustrasi-tab">

						<div class="bg-white b-r-5 pt-2 pb-3 px-2">
							<div class="border-bottom mb-2">
								<div class="row" align="center">
									<div class="col-6 col-sm-6 col-md-3">
										<small class="grey">Saldo Tersedia</small>
										<h6>Rp. 2.500.000</h6>
									</div>
									<div class="col-6 col-sm-6 col-md-3">
										<small class="grey">Pendapatan Bersih</small>
										<h6>Rp. 3.500.000</h6>
									</div>
									<div class="col-6 col-sm-6 col-md-3">
										<small class="grey">Pendapatan telah dicairkan</small>
										<h6>Rp. 5.000.000</h6>
									</div>
									<div class="col-6 col-sm-6 col-md-3">
										<small class="grey">Pendapatan dalam proses</small>
										<h6>Rp. 1.500.000</h6>
									</div>
								</div>
							</div>
							
							<div class="btn-detail" align="right">
								<a  href="cairkan-saldo.php" class="bg-yellow f-12 b-r-30" role="button">
									<i class="fas fa-money-check-alt"></i> Cairkan saldo sekarang
								</a>
							</div>
						</div>

						<div class="mt-3 bg-white b-r-5 py-2 px-2">
							<div class="row">
								<div class="col-12 col-sm-12 col-md-1">
									<h6 class="pt-1">Filter</h6>
								</div>
								<div class="col-6 col-sm-12 col-md-3">
									<select class="form-control form-control-sm" id="">
										<option>Tahun</option>
										<option>2018</option>
										<option>2019</option>
										<option>2020</option>
									</select>					
								</div>
								<div class="col-6 col-sm-12 col-md-3">
									<select class="form-control form-control-sm" id="">
										<option>Bulan</option>
										<option>Januari</option>
										<option>Februari</option>
										<option>Maret</option>
										<option>April</option>
									</select>
								</div>
							</div>
						</div>

						<div class="mt-3 bg-white b-r-5 py-2 px-2">
							<div class="table-responsive">
								<table class="table table-striped font-table">
									<thead>
										<tr>
											<th scope="col">ID Pesanan</th>
											<th scope="col">Tanggal</th>
											<th scope="col">Pelanggan</th>
											<th scope="col">Ilustrasi</th>
											<th scope="col">Jenis </th>
											<th scope="col" style="text-align: right">Total</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<th scope="row">PI-90928-231</th>
											<td>10 Januari 2018</td>
											<td>Ferguso</td>
											<td>WPAP</td>
											<td>Digital</td>
											<td class="blue b-600" align="right">Rp.100.000</td>
										</tr>
										<tr>
											<th scope="row">PI-90928-231</th>
											<td>10 Januari 2018</td>
											<td>Ferguso</td>
											<td>WPAP</td>
											<td>Cetak Produk</td>
											<td class="blue b-600" align="right">Rp.100.000</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>

					</div>
					<div class="tab-pane fade" id="v-pills-koleksi" role="tabpanel" aria-labelledby="v-pills-koleksi-tab">

						<div class="bg-white b-r-5 pt-2 pb-3 px-2">
							<div class="border-bottom mb-2">
								<div class="row" align="center">
									<div class="col-6 col-sm-6 col-md-3">
										<small class="grey">Saldo Tersedia</small>
										<h6>Rp. 2.500.000</h6>
									</div>
									<div class="col-6 col-sm-6 col-md-3">
										<small class="grey">Pendapatan Bersih</small>
										<h6>Rp. 3.500.000</h6>
									</div>
									<div class="col-6 col-sm-6 col-md-3">
										<small class="grey">Pendapatan telah dicairkan</small>
										<h6>Rp. 5.000.000</h6>
									</div>
									<div class="col-6 col-sm-6 col-md-3">
										<small class="grey">Pendapatan dalam proses</small>
										<h6>Rp. 1.500.000</h6>
									</div>
								</div>
							</div>
							
							<div class="btn-detail" align="right">
								<a  href="cairkan-saldo.php" class="bg-yellow f-12 b-r-30" role="button">
									<i class="fas fa-money-check-alt"></i> Cairkan saldo sekarang
								</a>
							</div>
						</div>

						<div class="mt-3 bg-white b-r-5 py-2 px-2">
							<div class="row">
								<div class="col-12 col-sm-12 col-md-1">
									<h6 class="pt-1">Filter</h6>
								</div>
								<div class="col-6 col-sm-12 col-md-3">
									<select class="form-control form-control-sm" id="">
										<option>Tahun</option>
										<option>2018</option>
										<option>2019</option>
										<option>2020</option>
									</select>					
								</div>
								<div class="col-6 col-sm-12 col-md-3">
									<select class="form-control form-control-sm" id="">
										<option>Bulan</option>
										<option>Januari</option>
										<option>Februari</option>
										<option>Maret</option>
										<option>April</option>
									</select>
								</div>
							</div>
						</div>

						<div class="mt-3 bg-white b-r-5 py-2 px-2">
							<div class="table-responsive">
								<table class="table table-striped font-table">
									<thead>
										<tr>
											<th scope="col">ID Pesanan</th>
											<th scope="col">Tanggal</th>
											<th scope="col">Pelanggan</th>
											<th scope="col">Ilustrasi</th>
											<th scope="col">Jenis </th>
											<th scope="col" style="text-align: right">Total</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<th scope="row">PI-90928-231</th>
											<td>10 Januari 2018</td>
											<td>Ferguso</td>
											<td>WPAP</td>
											<td>MUG</td>
											<td class="blue b-600" align="right">Rp.100.000</td>
										</tr>
										<tr>
											<th scope="row">PI-90928-231</th>
											<td>10 Januari 2018</td>
											<td>Ferguso</td>
											<td>WPAP</td>
											<td>Bantal</td>
											<td class="blue b-600" align="right">Rp.100.000</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>

					</div>
					<div class="tab-pane fade" id="v-pills-riwayat" role="tabpanel" aria-labelledby="v-pills-riwayat-tab">

						<div class="bg-white b-r-5 py-2 px-2">
							<div class="row">
								<div class="col-12 col-sm-12 col-md-1" style="padding-top: 5px;">
									<h6>Filter</h6>
								</div>
								<div class="col-12 col-sm-12 col-md-3" style="margin-bottom: 15px">
									<div class="list3">
										<select class="form-control form-control-sm" id="">
											<option>Tahun</option>
											<option>2</option>
											<option>3</option>
											<option>4</option>
											<option>5</option>
										</select>
									</div>										
								</div>
								<div class="col-12 col-sm-12 col-md-3">
									<div class="list3">
										<select class="form-control form-control-sm" id="">
											<option>Bulan</option>
											<option>2</option>
											<option>3</option>
											<option>4</option>
											<option>5</option>
										</select>
									</div>	
								</div>
								<div class="col-12 col-sm-12 col-md-3">
									<div class="list3">
										<select class="form-control form-control-sm" id="">
											<option>Ilustrasi</option>
											<option>Koleksi</option>
										</select>
									</div>	
								</div>
							</div>
						</div>

						<div>
							<div class="table-responsive">
								<table class="table table-striped font-table">
									<thead>
										<tr>
											<th scope="col">ID Transaksi</th>
											<th scope="col">Tanggal</th>
											<th scope="col" style="text-align: right">Total</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<th scope="row">PI-90928-231</th>
											<td>10 Januari 2018</td>
											<td class="blue b-600" align="right">Rp.150.000</td>
										</tr>
										<tr>
											<th scope="row">PI-90928-231</th>
											<td>10 Januari 2018</td>
											<td class="blue b-600" align="right">Rp.100.000</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>

					</div>
				</div>

			</div>
		</div>
	</div>
</div>




<?php
include "footer-dashboard.php";
?>
<?php
include "footer.php";
?>