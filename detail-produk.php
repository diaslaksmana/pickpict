<?php
include "header.php";
?>
<?php
include "header2.php";
?>

<div id="detail-product" class="bg-color-grey">
	<div class="container">
		<div class="detail-product bg-white">
			<div class="row">
				<div class="col-12 col-sm-12 col-md-4 col-lg-4">
					<div class="img-display">
						<div class="">
							<div class="row pt-4">
								<div class="col-12">
									<div id="lotus" class="picture" style="display:none">
										<img class="img-fluid" src="assets/img/s2.jpg"  alt="Lotus">
									</div>

									<div id="boat" class="picture" style="display:none">
										<img class="img-fluid" src="assets/img/s3.jpg" alt="Boat">
									</div>
									<div id="leaves" class="picture" style="display:none">
										<img class="img-fluid" src="assets/img/s6.jpg" alt="Leaves">
									</div>
								</div>
							</div>
						</div>
						<div class="row mt-2 mb-4">
							<div class="col-4 col-sm-4 col-md-4 col-lg-4">
								<a href="javascript:void(0)" class="myhover" onclick="openImg('lotus');">
									<img class="img-fluid" src="assets/img/s2.jpg" alt="Lotus">
								</a>
							</div>
							<div class="col-4 col-sm-4 col-md-4 col-lg-4">
								<a href="javascript:void(0)" class="myhover" onclick="openImg('boat');">
									<img class="img-fluid" src="assets/img/s3.jpg" alt="Boat">
								</a>
							</div>
							<div class="col-4 col-sm-4 col-md-4 col-lg-4">
								<a href="javascript:void(0)" class="myhover" onclick="openImg('leaves');">
									<img class="img-fluid" src="assets/img/s6.jpg" alt="Leaves">
								</a>
							</div>
						</div>
					</div>	
				</div>
				<div class="col-12 col-sm-12 col-md-8 col-lg-8">
					<div class="row">
						<div class="col-8 col-sm-6 col-md-6">
							<h5>John Lennon</h5>
						</div>
						<div class="col-4 col-sm-6 col-md-6" align="right">
							<div class="detail-produk-right">
								<a href="">
									<i class="far fa-heart"></i>
								</a>
								<a href="">
									<i class="fas fa-share-alt"></i>
								</a>
							</div>							
						</div>
					</div>
					<div class="info-creator">
						<div class="avatar-creator">
							<img src="assets/img/avatar.png">
						</div>
						<div class="name-creator">
							<h6><span>Karya dari</span><a href="creator.php"> John dae</a></h6>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="info-product mt-2">
						<div class="row">
							<div class="col-12 col-sm-3 col-md-3 col-lg-3">
								<label>Type : <span> Kaos</span></label>
							</div>
							<div class="col-12 col-sm-3 col-md-3 col-lg-3">
								<label>Dilihat : <span> 100</span></label>
							</div>
							<div class="col-12 col-sm-3 col-md-3 col-lg-3">
								<label>Terjual : <span> 150</span></label>
							</div>
						</div>
					</div>
					<div class="description-product mt-2">
						<h6>Deskripsi</h6>
						<p>
							John Winston Lennon (lahir di Liverpool, Inggris, 9 Oktober 1940 – meninggal di New York City, Amerika Serikat, 8 Desember 1980 pada umur 40 tahun) paling dikenal sebagai penyanyi
						</p>						
					</div>
					<div>
						<div class="row">
							<div class="col-12 col-sm-12 col-md-12 col-lg-5 col-xl-4 pt-2">
								<h6><b>Pilih Produk yang tersedia :</b></h6>
							</div>
							<div class="col-12 col-sm-4 col-md-4 col-lg-3 col-xl-3">
								<div class="form-group">
									<select class="form-control form-control-sm" id="">
										<option>Kaos</option>
										<option>Bantal</option>
										<option>Mug</option>
										<option>Case HP</option>
									</select>
								</div>
							</div>
							<div class="col-12 col-sm-4 col-md-4 col-lg-2 col-xl-3">
								<input type="number" class="form-control form-control-sm" id="" placeholder="Jumlah" min="0">
							</div>
						</div>
					</div>
					<div class="size-product mt-2">
						<h6>Pilih Ukuran</h6>
						<div class="row">
							<div class="col-6 col-sm-6 col-md-3 " align="center">
								<img src="assets/img/kaos.jpg" >
								<h5>M</h5>
								<input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios1" value="option1" checked>
							</div>
							<div class="col-6 col-sm-6 col-md-3" align="center">
								<img src="assets/img/kaos.jpg" >
								<h5>L</h5>
								<input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios2" value="option2">
							</div>
							<div class="col-6 col-sm-6 col-md-3">

							</div>
						</div>
						<p class="mt-4">
							Dikirim dalam 2-3 Hari kerja
						</p>
					</div>

					<div class="">
						<div class="row">
							<div class="col-12 col-sm-6 col-md-6 col-lg-4">
								<h3 class="red">
									<b>Rp. 150.000</b>
								</h3>
							</div>
							<div class="col-12 col-sm-6 col-md-6 col-lg-8">
								<div class="btn-add-to-cart">
									<button type="submit" class="btn btn-sm"><i class="fas fa-shopping-bag"></i> Tambahke keranjang</button>
								</div>								
							</div>
						</div>
					</div>
				</div>
			</div>

			<!--Koleksi lainnya-->

			<div class="row mt-5">
				<div class="col-6 coll-sm-6 col-md-6">
					<h6><b>Koleksi lainnya John Dae </b></h6>
				</div>
				<div class="col-6 coll-sm-6 col-md-6" align="right">
					<div class="btn-view-all">
						<a href="">Selengkapnya</a>
					</div>
				</div>
			</div>

			<div class="row mt-4">
				<div class="col-12 col-sm-6 col-md-6 col-lg-3">
					<div class="postcard">					
						<div class="cover100">
							<span>
								Rp 120.000
							</span>
							<img src="assets/img/kaos.jpg">
						</div>
						<div class="postcard-body">
							<a href="detail-produk.php">
								<h5>John Lennon</h5>
								<h6>WPAP</h6>
								<small>by John Dae</small>
							</a>						
						</div>
						<div class="postcard-footer">
							<label>
								<i class="far fa-eye"></i> 100
							</label>
							<label>
								<i class="fas fa-heart"></i> 50
							</label>
							<label>
								<i class="fab fa-twitch"></i> 80
							</label>
						</div>
					</div>
				</div>
				<div class="col-12 col-sm-6 col-md-6 col-lg-3">
					<div class="postcard">					
						<div class="cover100">
							<span>
								Rp 50.000
							</span>
							<img src="assets/img/mug.jpeg">
						</div>
						<div class="postcard-body">
							<a href="">
								<h5>Taylor Swift</h5>
								<h6>Mug</h6>
								<small>by John Dae</small>
							</a>						
						</div>
						<div class="postcard-footer">
							<label>
								<i class="far fa-eye"></i> 100
							</label>
							<label>
								<i class="fas fa-heart"></i> 50
							</label>
							<label>
								<i class="fab fa-twitch"></i> 80
							</label>
						</div>
					</div>
				</div>
				<div class="col-12 col-sm-6 col-md-6 col-lg-3">
					<div class="postcard">					
						<div class="cover100">
							<span>
								Rp 100.000
							</span>
							<img src="assets/img/bantal.jpg">
						</div>
						<div class="postcard-body">
							<a href="">
								<h5>Women</h5>
								<h6>Bantal</h6>
								<small>by John Dae</small>
							</a>						
						</div>
						<div class="postcard-footer">
							<label>
								<i class="far fa-eye"></i> 100
							</label>
							<label>
								<i class="fas fa-heart"></i> 50
							</label>
							<label>
								<i class="fab fa-twitch"></i> 80
							</label>
						</div>
					</div>
				</div>
				<div class="col-12 col-sm-6 col-md-6 col-lg-3">
					<div class="postcard">					
						<div class="cover100">
							<span>
								Rp 120.000
							</span>
							<img src="assets/img/casehp.jpg">
						</div>
						<div class="postcard-body">
							<a href="">
								<h5>OASIS</h5>
								<h6>WPAP</h6>
								<small>by John Dae</small>
							</a>						
						</div>
						<div class="postcard-footer">
							<label>
								<i class="far fa-eye"></i> 100
							</label>
							<label>
								<i class="fas fa-heart"></i> 50
							</label>
							<label>
								<i class="fab fa-twitch"></i> 80
							</label>
						</div>
					</div>
				</div>
			</div>

			<div class="coment-product my-3">
				<div class="mb-4" align="left">
					<h6><b>Ulasan Produk</b></h6>
				</div>

				<div class="owl-carousel owl-theme mb-3">
					<div class="item">
						<div class="box">
							<div class="row">
								<div class="col-3 col-sm-3 col-md-3">
									<div class="avatar2">
										<img src="assets/img/avatar.png">
									</div>
								</div>
								<div class="col-9 col-sm-9 col-md-9">
									<h6>Chris Evans</h6>
									<small>23 Maret 2019</small>
								</div>
							</div>
							<div class="rating mt-3" align="center">
								<label>
									<i class="fas fa-star"></i>
									<i class="fas fa-star"></i>
									<i class="fas fa-star"></i>
									<i class="fas fa-star"></i>
									<i class="fas fa-star"></i>
								</label>
							</div>
							<p align="center">
								Bagus sekali pengerjaannya, memuaskan, top !
							</p>
						</div>
					</div>
					<div class="item">
						<div class="box">
							<div class="row">
								<div class="col-3 col-sm-3 col-md-3">
									<div class="avatar2">
										<img src="assets/img/avatar.png">
									</div>
								</div>
								<div class="col-9 col-sm-9 col-md-9">
									<h6>Robert Downey</h6>
									<small>23 Maret 2019</small>
								</div>
							</div>
							<div class="rating mt-3" align="center">
								<label>
									<i class="fas fa-star"></i>
									<i class="fas fa-star"></i>
									<i class="fas fa-star"></i>
									<i class="fas fa-star"></i>
									<i class="fas fa-star"></i>
								</label>
							</div>
							<p align="center">
								Bagus sekali pengerjaannya, memuaskan, top !
							</p>
						</div>
					</div>
					<div class="item">
						<div class="box">
							<div class="row">
								<div class="col-3 col-sm-3 col-md-3">
									<div class="avatar2">
										<img src="assets/img/avatar.png">
									</div>
								</div>
								<div class="col-9 col-sm-9 col-md-9">
									<h6>Tcakala</h6>
									<small>23 Maret 2019</small>
								</div>
							</div>
							<div class="rating mt-3" align="center">
								<label>
									<i class="fas fa-star"></i>
									<i class="fas fa-star"></i>
									<i class="fas fa-star"></i>
									<i class="fas fa-star"></i>
									<i class="fas fa-star"></i>
								</label>
							</div>
							<p align="center">
								Bagus sekali pengerjaannya, memuaskan, top !
							</p>
						</div>
					</div>
					<div class="item">
						<div class="box">
							<div class="row">
								<div class="col-3 col-sm-3 col-md-3">
									<div class="avatar2">
										<img src="assets/img/avatar.png">
									</div>
								</div>
								<div class="col-9 col-sm-9 col-md-9">
									<h6>Chris Evans</h6>
									<small>23 Maret 2019</small>
								</div>
							</div>
							<div class="rating mt-3" align="center">
								<label>
									<i class="fas fa-star"></i>
									<i class="fas fa-star"></i>
									<i class="fas fa-star"></i>
									<i class="fas fa-star"></i>
									<i class="fas fa-star"></i>
								</label>
							</div>
							<p align="center">
								Bagus sekali pengerjaannya, memuaskan, top !
							</p>
						</div>
					</div>
					<div class="item">
						<div class="box">
							<div class="row">
								<div class="col-3 col-sm-3 col-md-3">
									<div class="avatar2">
										<img src="assets/img/avatar.png">
									</div>
								</div>
								<div class="col-9 col-sm-9 col-md-9">
									<h6>Chris Evans</h6>
									<small>23 Maret 2019</small>
								</div>
							</div>
							<div class="rating mt-3" align="center">
								<label>
									<i class="fas fa-star"></i>
									<i class="fas fa-star"></i>
									<i class="fas fa-star"></i>
									<i class="fas fa-star"></i>
									<i class="fas fa-star"></i>
								</label>
							</div>
							<p align="center">
								Bagus sekali pengerjaannya, memuaskan, top !
							</p>
						</div>
					</div>
				</div>

			</div>

		</div>
	</div>
</div>

<script type="text/javascript">
	//tab gallery
	openImg("lotus");

	function openImg(imgName) {
		var i, x;
		x = document.getElementsByClassName("picture");
		for (i = 0; i < x.length; i++) {
			x[i].style.display = "none";
		}
		document.getElementById(imgName).style.display = "block";
	};
</script>
<?php
include "footer2.php";
?>
<?php
include "footer.php";
?>