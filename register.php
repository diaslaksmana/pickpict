<?php
include "header.php";
?>

<div id="register">
	<div class="container">
		<div class="register">
			<div class="row register-box-shadow">
				<div class="col-12 col-12 col-md-5 col-lg-5 bg-color-blue">
					<h3 class="py-3">
						Daftar Sekarang
					</h3>
					<div class="img-register">
						<img src="assets/img/register.svg">
					</div>
				</div>
				<div class="col-12 col-12 col-md-7 col-lg-7 pt-3">
					<form>
						<center>                
							<div class="floating-form mt-3">
								<div class="floating-label">      
									<input class="floating-input" type="email" placeholder=" ">
									<span class="highlight"></span>
									<label><i class="fas fa-envelope"></i> E-Mail</label>
								</div>

								<div class="floating-label">      
									<input class="floating-input" type="text" placeholder=" ">
									<span class="highlight"></span>
									<label><i class="fas fa-user"></i> Nama</label>
								</div>

								<div class="floating-label">      
									<input class="floating-input" type="password" placeholder=" ">
									<span class="highlight"></span>
									<label><i class="fas fa-lock"></i> Password</label>
								</div> 
							</div>

							<div class="register-as" align="left">
								<h6>Daftar sebagai :</h6>
								<div class="row">
									<div class="col-12 col-sm-4 col-md-3">
										<input type="radio" id="radio01" name="radio" />
										<label for="radio01"><span></span>Pembeli</label>
									</div>

									<div class="col-12 col-sm-4 col-md-3">
										<input type="radio" id="radio02" name="radio" />
										<label for="radio02"><span></span>Creator</label>
									</div>
								</div>
							</div>
							
							<div class="btn-register mb-3">
								<a  href="index.php"  class="btn btn-secondary btn-sm" data-dismiss="modal">Kembali</a>
								<button type="submit" class="btn btn-blue btn-sm">Daftar</button>
							</div>
							<div class="forget-password">
								<p>Dengan mendaftar, anda sudah menyetujui <a href=""> Syarat dan Ketentuan</a> milik Pickpict.</p>
							</div>
						</center>
					</form>
				</div>
			</div>
		</div>		
	</div>
</div>

<?php
include "footer.php";
?>