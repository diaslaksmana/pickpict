<?php
include "header.php";
?>
<?php
include "header2.php";
?>

<div id="page-creator" class="bg-color-grey">
	<div class="container">
		<div class="row">
			<div class="col-12 col-sm-12 col-md-4 col-lg-3">
				<figure class="snip1559">
					<div class=" verified">
						<span>
							<i class="fas fa-check-circle"></i>  verified
						</span>
					</div>
					<div class="profile-image">
						<img src="assets/img/profil2.jpg" alt="profile-sample2" />
					</div>
					<figcaption>
						<h3>John Dae</h3>
						<div class="location pt-2">
							<p>
								<i class="fas fa-map-marker-alt"></i> Surakarta
							</p>							
						</div>						
					</figcaption>
				</figure>
			</div>
			<div class="col-12 col-sm-12 col-md-8 col-lg-9">
				<div class="description-creator bg-white">
					<div class="rating-creator">
						<div class="row">
							<div class="col-9 col-sm-9 col-md-9 col-lg-9">
								<div class="">
									<div class="rating2">
										<h6 class="grey">
											Rating : 
											<span>
												<i class="fas fa-star"></i>
												<i class="fas fa-star"></i>	
												<i class="fas fa-star"></i>
												<i class="fas fa-star"></i>
												<i class="fas fa-star"></i>
											</span> 
											<label><i> (60 Ulasan) </i></label>
										</h6>
									</div>					
								</div>
							</div>
							<div class="col-3 col-sm-3 col-md-3 col-lg-3" align="right">
								<div class="like">	
									<i class="far fa-heart"></i>
								</div>							
							</div>
						</div>						
						<div class="row mt-2">
							<div class="col-12 col-sm-12 col-md-12 col-lg-3 col-xl-2">
								<div class="info2">
									<h6 class="grey">
										Dilihat : <span>100</span>
									</h6>
								</div>
							</div>
							<div class="col-12 col-sm-12 col-md-12 col-lg-3 col-xl-3">
								<div class="info2">
									<h6 class="grey">
										Penjualan : <span>120</span>
									</h6>
								</div>
							</div>
							<div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-4">
								<div class="info2">
									<h6 class="grey">
										Member sejak : <span>Desember 2017</span>
									</h6>
								</div>
							</div>
						</div>
					</div>
					<div class="bio mt-2">
						<h6>
							Freelance ilustrator since 2013
						</h6>
					</div>
					<div class="skill mt-4">
						<h6>
							Ilustrasi yang dibuat :<span> Cartoon, Line Art, Pop Art, Vector, WPAP</span>
						</h6>
					</div>
					<div class="action2 mt-3">
						<a href="">
							<i class="far fa-envelope"></i> Kirim Pesan
						</a>
						<a href="pesan-custom.php">
							<i class="fas fa-shopping-basket"></i> Pesan Custom
						</a>
					</div>
				</div>
			</div>
		</div>

		<!--Service-->

		<div class="row mt-5">
			<div class="col-6 coll-sm-6 col-md-6">
				<h5><b>Service </b></h5>
			</div>
			<div class="col-6 coll-sm-6 col-md-6" align="right">
				<div class="btn-view-all">
					<a href="">Selengkapnya</a>
				</div>
			</div>
		</div>

		<div class="row mt-4">
			<div class="col-12 col-sm-6 col-md-6 col-lg-3">
				<div class="postcard">					
					<div class="cover100">
						<img src="assets/img/kaos.jpg">
					</div>
					<div class="postcard-body">
						<a href="detail-produk.php">
							<h5>John Lennon</h5>
							<h6>WPAP</h6>
							<small>by John Dae</small>
						</a>						
					</div>
				</div>
			</div>
			<div class="col-12 col-sm-6 col-md-6 col-lg-3">
				<div class="postcard">					
					<div class="cover100">
						<img src="assets/img/mug.jpeg">
					</div>
					<div class="postcard-body">
						<a href="">
							<h5>Taylor Swift</h5>
							<h6>Mug</h6>
							<small>by John Dae</small>
						</a>						
					</div>
				</div>
			</div>
			<div class="col-12 col-sm-6 col-md-6 col-lg-3">
				<div class="postcard">					
					<div class="cover100">
						<img src="assets/img/bantal.jpg">
					</div>
					<div class="postcard-body">
						<a href="">
							<h5>Women</h5>
							<h6>Bantal</h6>
							<small>by John Dae</small>
						</a>						
					</div>
				</div>
			</div>
			<div class="col-12 col-sm-6 col-md-6 col-lg-3">
				<div class="postcard">					
					<div class="cover100">
						<img src="assets/img/casehp.jpg">
					</div>
					<div class="postcard-body">
						<a href="">
							<h5>OASIS</h5>
							<h6>WPAP</h6>
							<small>by John Dae</small>
						</a>						
					</div>
				</div>
			</div>
		</div>


		<!--Koleksi -->

		<div class="row mt-5">
			<div class="col-6 coll-sm-6 col-md-6">
				<h5><b>Koleksi</b></h5>
			</div>
			<div class="col-6 coll-sm-6 col-md-6" align="right">
				<div class="btn-view-all">
					<a href="">Selengkapnya</a>
				</div>
			</div>
		</div>

		<div class="row mt-4">
			<div class="col-12 col-sm-6 col-md-6 col-lg-3">
				<div class="postcard">					
					<div class="cover100">
						<span>
							Rp 120.000
						</span>
						<img src="assets/img/kaos.jpg">
					</div>
					<div class="postcard-body">
						<a href="detail-produk.php">
							<h5>John Lennon</h5>
							<h6>WPAP</h6>
							<small>by John Dae</small>
						</a>						
					</div>
					<div class="postcard-footer">
						<label>
							<i class="far fa-eye"></i> 100
						</label>
						<label>
							<i class="fas fa-heart"></i> 50
						</label>
						<label>
							<i class="fab fa-twitch"></i> 80
						</label>
					</div>
				</div>
			</div>
			<div class="col-12 col-sm-6 col-md-6 col-lg-3">
				<div class="postcard">					
					<div class="cover100">
						<span>
							Rp 50.000
						</span>
						<img src="assets/img/mug.jpeg">
					</div>
					<div class="postcard-body">
						<a href="">
							<h5>Taylor Swift</h5>
							<h6>Mug</h6>
							<small>by John Dae</small>
						</a>						
					</div>
					<div class="postcard-footer">
						<label>
							<i class="far fa-eye"></i> 100
						</label>
						<label>
							<i class="fas fa-heart"></i> 50
						</label>
						<label>
							<i class="fab fa-twitch"></i> 80
						</label>
					</div>
				</div>
			</div>
			<div class="col-12 col-sm-6 col-md-6 col-lg-3">
				<div class="postcard">					
					<div class="cover100">
						<span>
							Rp 100.000
						</span>
						<img src="assets/img/bantal.jpg">
					</div>
					<div class="postcard-body">
						<a href="">
							<h5>Women</h5>
							<h6>Bantal</h6>
							<small>by John Dae</small>
						</a>						
					</div>
					<div class="postcard-footer">
						<label>
							<i class="far fa-eye"></i> 100
						</label>
						<label>
							<i class="fas fa-heart"></i> 50
						</label>
						<label>
							<i class="fab fa-twitch"></i> 80
						</label>
					</div>
				</div>
			</div>
			<div class="col-12 col-sm-6 col-md-6 col-lg-3">
				<div class="postcard">					
					<div class="cover100">
						<span>
							Rp 120.000
						</span>
						<img src="assets/img/casehp.jpg">
					</div>
					<div class="postcard-body">
						<a href="">
							<h5>OASIS</h5>
							<h6>WPAP</h6>
							<small>by John Dae</small>
						</a>						
					</div>
					<div class="postcard-footer">
						<label>
							<i class="far fa-eye"></i> 100
						</label>
						<label>
							<i class="fas fa-heart"></i> 50
						</label>
						<label>
							<i class="fab fa-twitch"></i> 80
						</label>
					</div>
				</div>
			</div>
		</div>

	</div>
</div>


<?php
include "footer2.php";
?>
<?php
include "footer.php";
?>