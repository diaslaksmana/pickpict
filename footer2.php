<div id="footer">
	<div class="container">
		<div class="footer-top">
			<div class="row">
				<div class="col-4 col-sm-6 col-md-2 col-lg-2">
					<h6><b>Pickpict</b></h6>
					<ul class="menu-footer">
						<li>
							<a href="">
								Tentang Kami
							</a>
						</li>
						<li>
							<a href="">
								Kontak
							</a>
						</li>
						<li>
							<a href="">
								FAQ
							</a>
						</li>
					</ul>
				</div>
				<div class="col-4 col-sm-6 col-md-2 col-lg-2">
					<h6><b>Beli</b></h6>
					<ul class="menu-footer">
						<li>
							<a href="">
								Pemesanan
							</a>
						</li>
						<li>
							<a href="">
								Pembayaran
							</a>
						</li>
					</ul>
				</div>
				<div class="col-4 col-sm-6 col-md-2 col-lg-1">
					<ul class="menu-footer"><b>Jual</b>
						<li>
							<a href="">
								Ilustrasi
							</a>
						</li>
						<li>
							<a href="">
								Produk
							</a>
						</li>
					</ul>
				</div>
				<div class="col-6 col-sm-6 col-md-4 col-lg-4">
					<div class="download-apps">
						<p>
							Makin Mudah dengan menggunakan aplikasi mobile <b> pickpict </b>
						</p>
						<a href="">
							<img src="assets/img/playstore.png">
						</a>
						<a href="">
							<img src="assets/img/apps.png">
						</a>
					</div>					
				</div>
				<div class="col-6 col-sm-12 col-md-12 col-lg-2">
					<h6><b>Temukan Kami di</b></h6>
					<ul class="sosmed">
						<li>
							<a href="">
								<i class="fab fa-instagram"></i>
							</a>
						</li>
						<li>
							<a href="">
								<i class="fab fa-twitter"></i>
							</a>
						</li>
						<li>
							<a href="">
								<i class="fab fa-facebook-f"></i>
							</a>
						</li>
					</ul>
				</div>
			</div>	
		</div>	
		<div class="footer-bottom pt-2">
			<p>
				Copyright © 2019 Pickpict . All Rights Reserved.
			</p>
		</div>	
	</div>	
</div>