<?php
include "header.php";
?>
<?php
include "header2.php";
?>


<div id="detail-product" class="bg-color-grey">
	<div class="container">
		<div class="upload bg-white">
			<div class="row ">
				<div class="col-4 col-sm-4 col-md-4 list5" align="right">
					<div class="progres step1">
						<h6> Pesanan</h6>
						<i class="fas fa-check-circle"></i>
					</div>
				</div>
				<div class="col-4 col-sm-4 col-md-4 list5" >
					<div class="progres " align="center">
						<h6><b>Upload</b></h6>
						<i class="fas fa-dot-circle"></i>
					</div>
				</div>
				<div class="col-4 col-sm-4 col-md-4 list5" align="left">
					<div class="progres step3">
						<h6>Detail</h6>
						<i class="fas fa-circle"></i>
					</div>
				</div>
			</div>

			<div class="row mt-5">
				<div class="col-12 col-sm-12 col-md-6 col-lg-3">
					<div class="img-ilustrasi">
						<h5 class="b-600">Ilustrasi : WPAP</h5>
						<img src="assets/img/wpap2.jpg">
					</div>
				</div>
				<div class="col-12 col-sm-12 col-md-6 col-lg-5">
					<h6 class="b-600">Silahkan Upload Foto</h6>
					<div class="main-wrapper">
						<div class="img-upload-plugin">
							<div class="img-upload-handler">
								<div class="img-preview-big">
									<img src="https://uploader-assets.s3.ap-south-1.amazonaws.com/codepen-default-placeholder.png">
									<div class="img-delete">
										<img src="https://uploader-assets.s3.ap-south-1.amazonaws.com/codepen-delete-icon.png">
									</div>
								</div>
							</div>
							<div class="img-preview-operate">
								<div class="img-holder" style="display: inline-block; padding: 2px;"></div>
								<button class="img-add-more">
									<img src="https://uploader-assets.s3.ap-south-1.amazonaws.com/codepen-add-more-btn.png">
								</button>
							</div>
							<input type="file" name="img-upload-input" style="display:none">
						</div>
					</div>
				</div>
				<div class="col-12 col-sm-12 col-md-6 col-lg-4">
					<div class="summary">
						<h6 class="b-600">Ringkasan Pesanan</h6>
						<div class="row">
							<div class="col-6 col-sm-6 col-md-6">
								<h5>Waktu Pengerjaan </h5>
							</div>
							<div class="col-6 col-sm-6 col-md-6" align="right">
								<h5> 4 Hari</h5>
							</div>
						</div>
						<div class="row">
							<div class="col-6 col-sm-6 col-md-6">
								<h5>Harga </h5>
							</div>
							<div class="col-6 col-sm-6 col-md-6" align="right">
								<h5> Rp. 150.000</h5>
							</div>
						</div>
						<div class="row">
							<div class="col-6 col-sm-6 col-md-6">
								<h5>Ongkir </h5>
							</div>
							<div class="col-6 col-sm-6 col-md-6" align="right">
								<h5> -</h5>
							</div>
						</div>
						<div class="row" >
							<div class="col-6 col-sm-6 col-md-6">
								<h5>Total Pembayaran </h5>
							</div>
							<div class="col-6 col-sm-6 col-md-6" align="right">
								<h5><b> Rp 150.000 </b></h5>
							</div>
						</div>
					</div>

					<div class="btn-order my-4">
						<a href="detail-pesanan.php" class="bg-yellow white">
							Selanjutnya
						</a>
					</div>

				</div>
			</div>
		</div>
	</div>
</div>

<?php
include "footer2.php";
?>
<?php
include "footer.php";
?>