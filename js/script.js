//sidemenu
function sideMenu() {
  document.getElementById("menuMobile").style.width = "250px";
}

function closeNav() {
  document.getElementById("menuMobile").style.width = "0%";
}

// Header scroll

$(function() {
  var header = $(".header");
  
  $(window).scroll(function() {    
    var scroll = $(window).scrollTop();
    if (scroll >= 50) {
      header.addClass("scrolled");
    } 
    else {
      header.removeClass("scrolled");
    }
  });
  
});

//header dashboard
function dashboardMenu() {
  document.getElementById("menuMobileDashboad").style.width = "250px";
}

function closeNav() {
  document.getElementById("menuMobileDashboad").style.width = "0%";
}

//
$(".hover").mouseleave(
  function () {
    $(this).removeClass("hover");
  }
  );

/* Demo purposes only */
var snippet = [].slice.call(document.querySelectorAll('.hover'));
if (snippet.length) {
  snippet.forEach(function (snippet) {
    snippet.addEventListener('mouseout', function (event) {
      if (event.target.parentNode.tagName === 'figure') {
        event.target.parentNode.classList.remove('hover')
      } else {
        event.target.parentNode.classList.remove('hover')
      }
    });
  });
}


// owl carousel
$('.owl-carousel').owlCarousel({
  margin:10,
  loop:true,
  autoWidth:true,
  items:3
});



// cetak produk
function show1(){
  document.getElementById('div1').style.display ='none';
}
function show2(){
  document.getElementById('div1').style.display = 'block';
}


//upload multiple
var initWidth = $(".img-preview-big")[0].offsetWidth;
$(".img-preview-big").css("height", initWidth + "px");

window.onresize = function(event) {
  var initWidth = $(".img-preview-big")[0].offsetWidth;
  $(".img-preview-big").css("height", initWidth + "px");
};

$(".img-upload-handler").on('mouseenter mouseleave', '.img-preview-big', function(ev) {
  var mouse_is_inside = ev.type === 'mouseenter';
  if ($(".img-preview-small").length > 0) {
    if (mouse_is_inside) {
      $(".img-delete").css("display", "flex");
    } else {
      $(".img-delete").css("display", "none");
    }
  } else {
    $(".img-delete").css("display", "none");
  }
});


$(".img-add-more").click(function() {
  $("input[type='file']").click();
})


$("input[type='file']").change(function() {
  var input = $("input[type='file']")[0];
  if (input.files && input.files[0]) {
    var reader = new FileReader();
    reader.onload = function(e) {
      $(".img-preview-big img")[0].src = e.target.result;
      $(".img-preview-small img").each(function() {
        $(this).removeClass("img-small-selected");
      })
      var newImg = '<div class="img-preview-small">' +
      '<img src="' + e.target.result + '" class="img-small-selected">' +
      '</div>';
      $(".img-holder").append(newImg);
      var left = $('.img-preview-operate').width();
      $('.img-preview-operate').scrollLeft(left);
    }
    reader.readAsDataURL(input.files[0]);
  }

});

// $(".img-preview-small").hover(function() {
//   console.log("Deepak"); 
// }, function() {
//   console.log("Chandwani");  
// })

$(document).on('mouseenter mouseleave', '.img-preview-small img', function(ev) {
  var mouse_is_inside = ev.type === 'mouseenter';
  if (mouse_is_inside) {
    $(this)[0].classList.add("img-small-active");
  } else {
    if (!$(this)[0].classList.contains("img-small-selected"));
    $(this)[0].classList.remove("img-small-active");
  }
});


$(document).on('click', '.img-preview-small img', function(ev) {
  $(".img-preview-small img").each(function() {
    $(this).removeClass("img-small-selected");
  })
  $(this).addClass("img-small-selected");
  $(".img-preview-big img")[0].src = $(this)[0].src;
});

$(".img-delete").click(function() {
  $(".img-small-selected")[0].parentElement.remove();
  if ($(".img-preview-small").length > 0) {
    $(".img-preview-small img")[0].classList.add("img-small-selected");
    $(".img-preview-big img")[0].src = $(".img-preview-small img")[0].src;
    $('.img-preview-operate').scrollLeft(0);
  } else {
    $(".img-preview-big img")[0].src = "https://uploader-assets.s3.ap-south-1.amazonaws.com/codepen-default-placeholder.png";
    $(".img-delete").css("display", "none");
  }
})




//checkbox upload koleksi
var loadFile = function(event) {
    var output = document.getElementById('output');
    output.src = URL.createObjectURL(event.target.files[0]);    
  };

  $(document).ready(function(){ 
    $("button").click(function (){
      $("input[type='file']").trigger('click');  
    }); 
  });

  $(document).ready(function(){
    $('input[type="checkbox"]').click(function(){
      var inputValue = $(this).attr("value");
      $("." + inputValue).toggle();
    });
  });