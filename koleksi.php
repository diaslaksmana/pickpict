<?php
include "header.php";
?>
<?php
include "header2.php";
?>

<div id="koleksi">
	<div class="container">
		<div class="title mb-5" align="center">
			<h2>Koleksi</h2>
		</div>
		<div class="row mb-2">
			<div class="col-6 colsm-6 col-md-6 col-lg-6">
				<h5>Bantal</h5>
			</div>
			<div class="col-6 colsm-6 col-md-6 col-lg-6" align="right">
				<div class="btn-view-all">
					<a href="">Selengkapnya</a>
				</div>
				
			</div>
		</div>
		<div class="row mb-5">
			<div class="col-6 col-sm-6 col-md-3 col-lg-3">
				<div class="koleksi-img">
					<a href="">
						<img src="assets/img/bantal.jpg">
					</a>					
				</div>
			</div>
			<div class="col-6 col-sm-6 col-md-3 col-lg-3">
				<div class="koleksi-img">
					<a href="">
						<img src="assets/img/bantal.jpg">
					</a>					
				</div>
			</div>
			<div class="col-6 col-sm-6 col-md-3 col-lg-3">
				<div class="koleksi-img">
					<a href="">
						<img src="assets/img/bantal.jpg">
					</a>					
				</div>
			</div>
			<div class="col-6 col-sm-6 col-md-3 col-lg-3">
				<div class="koleksi-img">
					<a href="">
						<img src="assets/img/bantal.jpg">
					</a>					
				</div>
			</div>
		</div>

		<div class="row mb-2">
			<div class="col-6 colsm-6 col-md-6 col-lg-6">
				<h5>Case HP</h5>
			</div>
			<div class="col-6 colsm-6 col-md-6 col-lg-6" align="right">
				<div class="btn-view-all">
					<a href="">Selengkapnya</a>
				</div>
				
			</div>
		</div>
		<div class="row mb-5">
			<div class="col-6 col-sm-6 col-md-3 col-lg-3">
				<div class="koleksi-img">
					<a href="">
						<img src="assets/img/casehp.jpg">
					</a>					
				</div>
			</div>
			<div class="col-6 col-sm-6 col-md-3 col-lg-3">
				<div class="koleksi-img">
					<a href="">
						<img src="assets/img/casehp.jpg">
					</a>					
				</div>
			</div>
			<div class="col-6 col-sm-6 col-md-3 col-lg-3">
				<div class="koleksi-img">
					<a href="">
						<img src="assets/img/casehp.jpg">
					</a>					
				</div>
			</div>
			<div class="col-6 col-sm-6 col-md-3 col-lg-3">
				<div class="koleksi-img">
					<a href="">
						<img src="assets/img/casehp.jpg">
					</a>					
				</div>
			</div>
		</div>


		<div class="row mb-2">
			<div class="col-6 colsm-6 col-md-6 col-lg-6">
				<h5>MUG</h5>
			</div>
			<div class="col-6 colsm-6 col-md-6 col-lg-6" align="right">
				<div class="btn-view-all">
					<a href="">Selengkapnya</a>
				</div>				
			</div>
		</div>
		<div class="row mb-5">
			<div class="col-6 col-sm-6 col-md-3 col-lg-3">
				<div class="koleksi-img">
					<a href="">
						<img src="assets/img/mug.jpeg">
					</a>					
				</div>
			</div>
			<div class="col-6 col-sm-6 col-md-3 col-lg-3">
				<div class="koleksi-img">
					<a href="">
						<img src="assets/img/mug.jpeg">
					</a>					
				</div>
			</div>
			<div class="col-6 col-sm-6 col-md-3 col-lg-3">
				<div class="koleksi-img">
					<a href="">
						<img src="assets/img/mug.jpeg">
					</a>					
				</div>
			</div>
			<div class="col-6 col-sm-6 col-md-3 col-lg-3">
				<div class="koleksi-img">
					<a href="">
						<img src="assets/img/mug.jpeg">
					</a>					
				</div>
			</div>
		</div>

	</div>
</div>


<?php
include "footer2.php";
?>
<?php
include "footer.php";
?>