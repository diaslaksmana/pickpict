<?php
include "header.php";
?>
<?php
include "header2.php";
?>

<div id="page-search">
	<div class="container">
		<div class="title-page">
			<h5>
				Pencarian terkait : <span> John Dae </span>
			</h5>
		</div>
		<div class="box-filter bg-color-filter">
			<form>
				<div class="row">
					<div class="12 col-sm-12 col-md-2 col-lg-2">
						<div class="form-group">
							<label>Ilustrasi</label>
							<select class="form-control form-control-sm" id="">
								<option>WPAP</option>
								<option>Vektor</option>
								<option>POP Art</option>
								<option>Kartun</option>
							</select>
						</div>
					</div>
					<div class="12 col-sm-12 col-md-2 col-lg-2">
						<div class="form-group">
							<label>Produk</label>
							<select class="form-control form-control-sm" id="">
								<option>Case HP</option>
								<option>Kaos</option>
								<option>Mug</option>
								<option>Tote Bag</option>
							</select>
						</div>
					</div>
					<div class="12 col-sm-12 col-md-3 col-lg-3">
						<div class="form-group">
							<label>Berdasarkan</label>
							<select class="form-control form-control-sm" id="">
								<option>Terbaru</option>
								<option>Dilihat</option>
								<option>Terjual</option>
							</select>
						</div>
					</div>
					<div class="12 col-sm-12 col-md-3 col-lg-3">
						<div class="slidecontainer">							
							<p>Range Harga: <span id="rangeOutput"></span></p>
							<input type="range" min="50000" max="300000" value="100000" class="slider" id="myRange">
						</div>
					</div>
					<div class="12 col-sm-12 col-md-2 col-lg-2">
						<div class="btn-filter">
							<button type="submit" class="btn btn-sm btn-info">Filter</button>
						</div>
						
					</div>
				</div>	
			</form>
		</div>		

		<div class="row">
			<div class="col-12 col-sm-6 col-md-6 col-lg-3">
				<div class="postcard">					
					<div class="cover100">
						<span>
							Rp 120.000
						</span>
						<img src="assets/img/kaos.jpg">
					</div>
					<div class="postcard-body">
						<a href="detail-produk.php">
							<h5>John Lennon</h5>
							<h6>WPAP</h6>
							<small>by John Dae</small>
						</a>						
					</div>
					<div class="postcard-footer">
						<label>
							<i class="far fa-eye"></i> 100
						</label>
						<label>
							<i class="fas fa-heart"></i> 50
						</label>
						<label>
							<i class="fab fa-twitch"></i> 80
						</label>
					</div>
				</div>
			</div>
			<div class="col-12 col-sm-6 col-md-6 col-lg-3">
				<div class="postcard">					
					<div class="cover100">
						<span>
							Rp 50.000
						</span>
						<img src="assets/img/mug.jpeg">
					</div>
					<div class="postcard-body">
						<a href="">
							<h5>Taylor Swift</h5>
							<h6>Mug</h6>
							<small>by John Dae</small>
						</a>						
					</div>
					<div class="postcard-footer">
						<label>
							<i class="far fa-eye"></i> 100
						</label>
						<label>
							<i class="fas fa-heart"></i> 50
						</label>
						<label>
							<i class="fab fa-twitch"></i> 80
						</label>
					</div>
				</div>
			</div>
			<div class="col-12 col-sm-6 col-md-6 col-lg-3">
				<div class="postcard">					
					<div class="cover100">
						<span>
							Rp 100.000
						</span>
						<img src="assets/img/bantal.jpg">
					</div>
					<div class="postcard-body">
						<a href="">
							<h5>Women</h5>
							<h6>Bantal</h6>
							<small>by John Dae</small>
						</a>						
					</div>
					<div class="postcard-footer">
						<label>
							<i class="far fa-eye"></i> 100
						</label>
						<label>
							<i class="fas fa-heart"></i> 50
						</label>
						<label>
							<i class="fab fa-twitch"></i> 80
						</label>
					</div>
				</div>
			</div>
			<div class="col-12 col-sm-6 col-md-6 col-lg-3">
				<div class="postcard">					
					<div class="cover100">
						<span>
							Rp 120.000
						</span>
						<img src="assets/img/casehp.jpg">
					</div>
					<div class="postcard-body">
						<a href="">
							<h5>OASIS</h5>
							<h6>WPAP</h6>
							<small>by John Dae</small>
						</a>						
					</div>
					<div class="postcard-footer">
						<label>
							<i class="far fa-eye"></i> 100
						</label>
						<label>
							<i class="fas fa-heart"></i> 50
						</label>
						<label>
							<i class="fab fa-twitch"></i> 80
						</label>
					</div>
				</div>
			</div>
		</div>


	</div>
</div>


<!-- Modal Buy Product -->
<div class="modal fade" id="buyProduct" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				...
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-primary">Save changes</button>
			</div>
		</div>
	</div>
</div>


<?php
include "footer2.php";
?>
<?php
include "footer.php";
?>