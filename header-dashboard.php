<div  id="header-dashboard" class="header-dashboard bg-blue">
	<div class="container">
		<div class="row pt-2">
			<div class="col-3 col-sm-3 col-md-2 col-lg-2">
				<div class="brand-logo mt-1">
					<a href="index.php">
						<h4 style="color: #fff; font-weight: bold;">PICKPICT</h4>
					</a>					
				</div>
			</div>
			<div class="col-9 col-sm-9 col-md-10 col-lg-10 " align="right">
				<ul class="menu-dashboard mt-2">
					<li class="hide-menu-mobile">
						<a href="dashboard.php">
							Dashboard
						</a>
					</li>
					<li class="hide-menu-mobile">
						<a href="pesanan.php">
							Pesanan
						</a>
					</li>
					<li class="hide-menu-mobile">
						<a href="pembelian.php">
							Pembelian
						</a>
					</li>
					<li class="hide-menu-mobile">
						<a href="portofolio.php">
							Portofolio
						</a>
					</li>
					<li class="hide-menu-mobile">
						<a href="pendapatan.php">
							Pendapatan
						</a>
					</li>
					<li >
						<a href="keranjang-belanja.php">
							<i class="fas fa-shopping-bag"></i>
							 <span class="badge">0</span>
						</a>
					</li>
					<li>
						<a href="kotak-pesan.php">
							<i class="fas fa-envelope"></i>
							<span><i class="fas fa-circle pink" ></i> </span> 
						</a>
					</li>
					<li>
						<a href="">
							<i class="fas fa-bell"></i>
							<span><i class="fas fa-circle yellow" ></i> </span> 
						</a>
					</li>
					<li class="hide-menu-mobile">
						<div class="dropdown">
							<div class="ava-profil border-left">
								<img src="assets/img/profil3.jpg">
							</div>
							<div class="dropdown-content">
								<a href="profil.php">Profil</a>
								<a href="favorit.php">Favorit</a>
								<a href="pengaturan.php">Pengaturan</a>
								<a href="#">Bantuan</a>
								<a href="#">Keluar</a>
							</div>
						</div>
					</li>
					<li class="p-t-m">
						<div class="icon-header-mobile">
							<i class="fas fa-bars" href="javascript:void(0)" class="closebtn" onclick="dashboardMenu()"></i>
						</div>
					</li>
				</ul>
			</div>
		</div>
	</div>	


	<!-- sidemenu -->

	<div id="menuMobileDashboad" class="overlay-dashboard">
		<a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
		<div class="overlay-content-dashboard">
			<div class="ava-picture">
				<a href="dashboard.php">
					<img src="assets/img/profil3.jpg"> John Dae					
				</a>
			</div>
			<div class="menu-list-dashboard mt-3">
				<ul>
					<li>
						<a href="">
							<i class="fas fa-user"></i> Profil
						</a>
					</li>
					<li>
						<a href="favorit.php">
							<i class="fas fa-heart"></i> Favorit
						</a>
					</li>
					<li>
						<a href="pesanan.php">
							<i class="far fa-handshake"></i> Pesanan
						</a>
					</li>
					<li>
						<a href="pembelian.php">
							<i class="fas fa-gift"></i> Pembelian
						</a>
					</li>
					<li>
						<a href="portofolio.php">
							<i class="fas fa-pencil-ruler"></i> Portofolio
						</a>
					</li>
					<li>
						<a href="pendapatan.php">
							<i class="fas fa-wallet"></i> Pendapatan
						</a>
					</li>
					<li>
						<a href="pengautran.php">
							<i class="fas fa-cog"></i> Pengaturan
						</a>
					</li>
					<li>
						<a href="">
							<i class="far fa-question-circle"></i> Bantuan
						</a>
					</li>
					<li>
						<a href="">
							<i class="fas fa-sign-out-alt"></i> Keluar
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>

</div>
