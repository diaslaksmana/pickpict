<?php
include "header.php";
?>
<?php
include "header-dashboard.php";
?>

<div id="dashboard" class="dashboard bg-grey">
	<div class="container">
		<div class="row">
			<div class="col-12 col-sm-12 col-md-3 col-lg-3">
				<div class="profil-picture bg-white mb-4" align="center">
					<div class="your-name mb-4" align="left">
						<h5>Halo,</h5>
						<h6>John Dae</h6>
					</div>
					
					<img src="assets/img/profil3.jpg">
					<h6 class="grey mt-3">Surakarta</h6>
					<small class="f-14">
						Freelancer since 2015
					</small>
				</div>

				<div class="bg-white mt-3 b-r-5 py-3 px-1" align="center">
					<h5 class="grey">Skill</h5>
					<p>
						WPAP, Line Art, Vektor, Pop Art
					</p>
				</div>
			</div>
			<div class="col-12 col-sm-12 col-md-9 col-lg-9">

				
				
				<div class="bg-white py-2 px-2 b-r-5">
					<!--Service-->

					<div class="pt-2 border-bottom">
						<h5><b>Service </b></h5>
					</div>

					<div class="row mt-4">
						<div class="col-12 col-sm-6 col-md-6 col-lg-3">
							<div class="postcard">					
								<div class="cover100">
									<img src="assets/img/kaos.jpg">
								</div>
								<div class="postcard-body">
									<a href="detail-produk.php">
										<h5>John Lennon</h5>
										<h6>WPAP</h6>
										<small>by John Dae</small>
									</a>						
								</div>
							</div>
						</div>
						<div class="col-12 col-sm-6 col-md-6 col-lg-3">
							<div class="postcard">					
								<div class="cover100">
									<img src="assets/img/mug.jpeg">
								</div>
								<div class="postcard-body">
									<a href="">
										<h5>Taylor Swift</h5>
										<h6>Mug</h6>
										<small>by John Dae</small>
									</a>						
								</div>
							</div>
						</div>
						<div class="col-12 col-sm-6 col-md-6 col-lg-3">
							<div class="postcard">					
								<div class="cover100">
									<img src="assets/img/bantal.jpg">
								</div>
								<div class="postcard-body">
									<a href="">
										<h5>Women</h5>
										<h6>Bantal</h6>
										<small>by John Dae</small>
									</a>						
								</div>
							</div>
						</div>
						<div class="col-12 col-sm-6 col-md-6 col-lg-3">
							<div class="postcard">					
								<div class="cover100">
									<img src="assets/img/casehp.jpg">
								</div>
								<div class="postcard-body">
									<a href="">
										<h5>OASIS</h5>
										<h6>WPAP</h6>
										<small>by John Dae</small>
									</a>						
								</div>
							</div>
						</div>
					</div>

					<div class="page-number">
						<nav aria-label="Page navigation ">
							<ul class="pagination justify-content-center">
								<li class="disabled">
									<a class="" href="#" tabindex="-1" aria-disabled="true">
										<i class="fas fa-chevron-circle-left"></i>
									</a>
								</li>
								<li class="active"><a class="" href="#">1</a></li>
								<li class=""><a class="" href="#">2</a></li>
								<li class=""><a class="" href="#">3</a></li>
								<li class=""><a class="" href="#">4</a></li>
								<li class=""><a class="" href="#">5</a></li>
								<li class="">
									<a class="" href="#">
										<i class="fas fa-chevron-circle-right"></i>
									</a>
								</li>
							</ul>
						</nav>	
					</div>
				</div>


				<div class="bg-white py-2 px-2 b-r-5 mt-4">
					<!--Koleksi-->

					<div class="pt-2 border-bottom">
						<h5><b>Koleksi </b></h5>
					</div>

					<div class="row mt-4">
						<div class="col-12 col-sm-6 col-md-6 col-lg-3">
							<div class="postcard">					
								<div class="cover100">
									<span>
										Rp 120.000
									</span>
									<img src="assets/img/kaos.jpg">
								</div>
								<div class="postcard-body">
									<a href="detail-produk.php">
										<h5>John Lennon</h5>
										<h6>WPAP</h6>
										<small>by John Dae</small>
									</a>						
								</div>
								<div class="postcard-footer">
									<label>
										<i class="far fa-eye"></i> 100
									</label>
									<label>
										<i class="fas fa-heart"></i> 50
									</label>
									<label>
										<i class="fab fa-twitch"></i> 80
									</label>
								</div>
							</div>
						</div>
						<div class="col-12 col-sm-6 col-md-6 col-lg-3">
							<div class="postcard">					
								<div class="cover100">
									<span>
										Rp 50.000
									</span>
									<img src="assets/img/mug.jpeg">
								</div>
								<div class="postcard-body">
									<a href="">
										<h5>Taylor Swift</h5>
										<h6>Mug</h6>
										<small>by John Dae</small>
									</a>						
								</div>
								<div class="postcard-footer">
									<label>
										<i class="far fa-eye"></i> 100
									</label>
									<label>
										<i class="fas fa-heart"></i> 50
									</label>
									<label>
										<i class="fab fa-twitch"></i> 80
									</label>
								</div>
							</div>
						</div>
						<div class="col-12 col-sm-6 col-md-6 col-lg-3">
							<div class="postcard">					
								<div class="cover100">
									<span>
										Rp 100.000
									</span>
									<img src="assets/img/bantal.jpg">
								</div>
								<div class="postcard-body">
									<a href="">
										<h5>Women</h5>
										<h6>Bantal</h6>
										<small>by John Dae</small>
									</a>						
								</div>
								<div class="postcard-footer">
									<label>
										<i class="far fa-eye"></i> 100
									</label>
									<label>
										<i class="fas fa-heart"></i> 50
									</label>
									<label>
										<i class="fab fa-twitch"></i> 80
									</label>
								</div>
							</div>
						</div>
						<div class="col-12 col-sm-6 col-md-6 col-lg-3">
							<div class="postcard">					
								<div class="cover100">
									<span>
										Rp 120.000
									</span>
									<img src="assets/img/casehp.jpg">
								</div>
								<div class="postcard-body">
									<a href="">
										<h5>OASIS</h5>
										<h6>WPAP</h6>
										<small>by John Dae</small>
									</a>						
								</div>
								<div class="postcard-footer">
									<label>
										<i class="far fa-eye"></i> 100
									</label>
									<label>
										<i class="fas fa-heart"></i> 50
									</label>
									<label>
										<i class="fab fa-twitch"></i> 80
									</label>
								</div>
							</div>
						</div>
					</div>

					<div class="page-number">
						<nav aria-label="Page navigation ">
							<ul class="pagination justify-content-center">
								<li class="disabled">
									<a class="" href="#" tabindex="-1" aria-disabled="true">
										<i class="fas fa-chevron-circle-left"></i>
									</a>
								</li>
								<li class="active"><a class="" href="#">1</a></li>
								<li class=""><a class="" href="#">2</a></li>
								<li class=""><a class="" href="#">3</a></li>
								<li class=""><a class="" href="#">4</a></li>
								<li class=""><a class="" href="#">5</a></li>
								<li class="">
									<a class="" href="#">
										<i class="fas fa-chevron-circle-right"></i>
									</a>
								</li>
							</ul>
						</nav>	
					</div>
				</div>
				
			</div>
		</div>
	</div>
</div>

<?php
include "footer-dashboard.php";
?>
<?php
include "footer.php";
?>