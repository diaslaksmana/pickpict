<?php
include "header.php";
?>
<?php
include "header2.php";
?>


<div id="panel-card" class="bg-color-grey">
	<div class="container">
		<div class="panel-card bg-white">
			<div class="title-info-buying" align="center">
				<h5>Silahkan lakukan pembayaran sebesar</h5>
				<h4 class="yellow b-600">Rp. 250.000,00</h4>
				<h6>Dengan cara transfer ke salah satu nomor rekening berikut :</h6>
			</div>
			<div class="bank-corporate">
				<div class="row ">
					<div class="col-12 col-sm-12 col-md-6 col-lg-6">
						<div class="row mb-4 ">
							<div class="col-12 col-sm-3 col-md-12 col-lg-3">
								<img src="assets/img/icon/bca.png" width="100%">
							</div>
							<div class="col-12 col-sm-9 col-md-12 col-lg-9">
								<h6>089079823606</h6>
								<h6>PT Karya Anak Bangsa</h6>
							</div>
						</div>
						<div class="row mb-4">
							<div class="col-12 col-sm-3 col-md-12 col-lg-3">
								<img src="assets/img/icon/mandiri.png" width="100%">
							</div>
							<div class="col-12 col-sm-9 col-md-12 col-lg-9">
								<h6>089079823606</h6>
								<h6>PT Karya Anak Bangsa</h6>
							</div>
						</div>
					</div>
					<div class="col-12 col-sm-12 col-md-6 col-lg-6 ">
						<div class="row mb-4">
							<div class="col-12 col-sm-3 col-md-12 col-lg-3">
								<img src="assets/img/icon/bni.png" width="100%">
							</div>
							<div class="col-12 col-sm-9 col-md-12 col-lg-9">
								<h6>089079823606</h6>
								<h6>PT Karya Anak Bangsa</h6>
							</div>
						</div>
						<div class="row mb-4">
							<div class="col-12 col-sm-3 col-md-12 col-lg-3">
								<img src="assets/img/icon/bri.png" width="100%">
							</div>
							<div class="col-12 col-sm-9 col-md-12 col-lg-9">
								<h6>089079823606</h6>
								<h6>PT Karya Anak Bangsa</h6>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="payment-time" align="center">
				<h4>Batas  waktu pembayaran</h4>
				<h5>ID Transaksi <b class="blue">PI-909709-2989-01</b></h5>
				<h6>2 Januari 2019, Pukul 18.00 </h6>
				<div class="time mt-3">
					<div class="row time m-t-t">
						<div class="col-4 col-sm-4 col-md-4 col-lg-5 day" align="right">
							<span> 0 </span> <span> 1 </span>
							<h6>jam</h6>
						</div>
						<div class="col-4 col-sm-3 col-md-3 col-lg-2">
							<span> 2 </span> <span> 0</span>
							<h6>Menit</h6>
						</div>
						<div class="col-4 col-sm-4 col-md-4 col-lg-5 minute" align="left">
							<span> 1 </span> <span> 5 </span>
							<h6>Detik</h6>
						</div>
					</div>
				</div>
			</div>

			<div class="info-payment mb-5" align="center">
				<p>
					Silahkan segera menyelesaikan pembayaran<br>
					agar pesanan kamu bisa segera dikerjakan dan dikirmkan. <br>
					Terima kasih
				</p>

				<a class="btn u" href="pembelian.php" role="button">Cek status pembayaran</a>
			</div>

		</div>
	</div>
</div>

<?php
include "footer2.php";
?>
<?php
include "footer.php";
?>