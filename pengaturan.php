<?php
include "header.php";
?>
<?php
include "header-dashboard.php";
?>

<div id="dashboard" class="dashboard bg-grey">
	<div class="container">
		<ul class="breadcrumb">
			<li><a href="dashboard.php">Dashboard</a></li>
			<li>Pengaturan</li>
		</ul>
		<div class="row">
			<div class="col-12 col-sm-12 col-md-3 col-lg-3">
				<div class="profil-picture bg-white mb-4" align="center">
					<div class="your-name mb-4" align="left">
						<h5>Halo,</h5>
						<h6>John Dae</h6>
					</div>
					
					<img src="assets/img/profil3.jpg">
					<h6 class="grey mt-3">Surakarta</h6>
					<small class="f-14">
						Freelancer since 2015
					</small>
				</div>

				<div class="bg-white mt-3 b-r-5 py-3 px-1" align="center">
					<h5 class="grey">Skill</h5>
					<p>
						WPAP, Line Art, Vektor, Pop Art
					</p>
				</div>
			</div>
			<div class="col-12 col-sm-12 col-md-9 col-lg-9">
				
				<div class="bg-white pb-3">

					<div class="nav-pills-border-blue">
						<div class="scroll">
							<ul class="nav mb-3" id="pills-tab" role="tablist">
								<li class="nav-item">
									<a class="nav-link active" id="pills-edit-profil-tab" data-toggle="pill" href="#pills-edit-profil" role="tab" aria-controls="pills-edit-profil" aria-selected="true">
										<i class="fas fa-user"></i> Edit Profil
									</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" id="pills-alamat-tab" data-toggle="pill" href="#pills-alamat" role="tab" aria-controls="pills-alamat" aria-selected="false">
										<i class="fas fa-map-marker-alt"></i> Alamat
									</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" id="pills-akun-bank-tab" data-toggle="pill" href="#pills-akun-bank" role="tab" aria-controls="pills-akun-bank" aria-selected="false">
										<i class="fas fa-university"></i> Akun Bank
									</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" id="pills-password-tab" data-toggle="pill" href="#pills-password" role="tab" aria-controls="pills-password" aria-selected="false">
										<i class="fas fa-unlock-alt"></i> Password
									</a>
								</li>
							</ul>
						</div>								
					</div>


					<div class="tab-content" id="pills-tabContent">
						<div class="tab-pane fade show active" id="pills-edit-profil" role="tabpanel" aria-labelledby="pills-edit-profil-tab">

							<div class="container">
								<div class="form-row">
									<div class="form-group col-md-6">
										<label >Nama</label>
										<div class="list3">
											<input type="text" class="form-control" id="" placeholder="Nama">
										</div>
									</div>
									<div class="form-group col-md-6">
										<label >Username</label>
										<div class="list3">
											<input type="text" class="form-control" id="" placeholder="E-Mail">
										</div>
									</div>
								</div>
								<div class="form-row">
									<div class="form-group col-md-6">
										<label >Telepon</label>
										<div class="list3">
											<input type="number" min="0" class="form-control" id="" placeholder="Telepon">
										</div>

									</div>
									<div class="form-group col-md-6">
										<label >Bio</label>
										<div class="list3">
											<input type="text" class="form-control" id="" placeholder="Bio Singkat">
										</div>
									</div>
								</div>
								<div class="form-group">
									<label >Skill</label>
									<div class="list3">
										<input type="text" class="form-control" id="" placeholder="Skill">
									</div>
								</div>

								<div class="btn-blue" align="center">
									<button type="submit" class="btn btn-sm btn-block">Simpan</button>
								</div>

							</div>
						</div>
						<div class="tab-pane fade" id="pills-alamat" role="tabpanel" aria-labelledby="pills-alamat-tab">

							<div class="container">
								<div class="form-group">
									<label>Provinsi</label>
									<div class="list3">
										<select class="form-control form-control-sm" id="">
											<option>Jawa tengah</option>
											<option>2</option>
											<option>3</option>
											<option>4</option>
											<option>5</option>
										</select>
									</div>
								</div>
								<div class="form-group">
									<label>Kota/Kabupaten</label>
									<div class="list3">
										<select class="form-control form-control-sm" id="">
											<option>Surakarta</option>
											<option>2</option>
											<option>3</option>
											<option>4</option>
											<option>5</option>
										</select>
									</div>
								</div>
								<div class="row">
									<div class="col-12 col-sm-12 col-md-6">
										<div class="form-group">
											<label>Kecamatan</label>
											<div class="list3">
												<select class="form-control form-control-sm" id="">
													<option>Jebres</option>
													<option>2</option>
													<option>3</option>
													<option>4</option>
													<option>5</option>
												</select>
											</div>
										</div>
									</div>
									<div class="col-12 col-sm-12 col-md-6">
										<div class="form-group">
											<label>Kodepos</label>
											<div class="list3">
												<input type="email" class="form-control form-control-sm" id="" placeholder="Kodepos">
											</div>
										</div>
									</div>
								</div>
								<div class="form-group">
									<label>Alamat Lengkap</label>
									<div class="list3">
										<textarea class="form-control form-control-sm" id="" rows="2" placeholder="Masukkan Jalan, Nomor rumah, RT/RW"></textarea>
									</div>
								</div>

								<div class="btn-blue" align="center">
									<button type="submit" class="btn btn-sm btn-block">Simpan</button>
								</div>
								
							</div>

						</div>
						<div class="tab-pane fade" id="pills-akun-bank" role="tabpanel" aria-labelledby="pills-akun-bank-tab">

							<div class="container">
								<div class="form-group">
									<label >Nama Bank</label>
									<div class="list3">
										<select class="form-control" id="exampleFormControlSelect1">
											<option>Bank BCA</option>
											<option>2</option>
											<option>3</option>
											<option>4</option>
											<option>5</option>
										</select>
									</div>
								</div>
								<div class="row">
									<div class="col-6 col-sm-6 col-md-6">
										<div class="form-group">
											<label >Nama</label>
											<div class="list3">
												<input type="text" class="form-control" id="" placeholder="Nama ">
											</div>
										</div>
									</div>
									<div class="col-6 col-sm-6 col-md-6">
										<div class="form-group">
											<label >Nomor Rekening</label>
											<div class="list3">
												<input type="number" min="0" class="form-control" id="" placeholder="Nomer Rekening">
											</div>
										</div>
									</div>
								</div>

								<div class="btn-blue" align="center">
									<button type="submit" class="btn btn-sm btn-block">Simpan</button>
								</div>
								
							</div>

						</div>
						<div class="tab-pane fade" id="pills-password" role="tabpanel" aria-labelledby="pills-password-tab">

							<div class="container">
								<div class="form-group">
									<label >Password Lama</label>
									<div class="row">
										<div class="col-6 ">
											<div class="list3">
												<input type="password" class="form-control" id="" placeholder="masukkan password lama ">
											</div>
										</div>
									</div>
								</div>
								<div class="form-group">
									<label >Pasword baru</label>
									<div class="row">
										<div class="col-6 ">
											<div class="list3">
												<input type="password" class="form-control" id="" placeholder="masukkan password baru ">
											</div>
										</div>
									</div>
								</div>

								<div class="btn-blue" align="center">
									<button type="submit" class="btn btn-sm btn-block">Simpan</button>
								</div>
								
							</div>

						</div>
					</div>

				</div>	
				
			</div>
		</div>
	</div>
</div>

<?php
include "footer-dashboard.php";
?>
<?php
include "footer.php";
?>