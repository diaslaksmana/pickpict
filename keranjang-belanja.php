<?php
include "header.php";
?>
<?php
include "header-dashboard.php";
?>

<div id="dashboard" class="dashboard bg-grey">
	<div class="container">
		<h5 class="b-600">Keranjang Belanja</h5>
		<div class="row">
			<div class="col-12 col-sm-12 col-md-8 col-lg-8">
				<div class="bg-white py-2 px-3 b-r-5 mb-3">
					<div class="row ">
						<div class="col-8 col-sm-6 col-md-6">
							<div class="form-check">
								<input type="checkbox" class="form-check-input" id="">
								<label class="form-check-label" ><b>Pilih Semua Transaksi</b></label>
							</div>
						</div>
						<div class="col-4 col-sm-6 col-md-6" align="right">
							<a class="blue f-12" href="#" role="button">Hapus Semua</a>
						</div>
					</div>
				</div>

				<h6>Pesanan Ilustrasi</h6>

				<div class="order-active bg-white">
					<div class="container"> 
						<div class="row scroll">
							<div class="col-1 col-md-1 col-lg-1 col-xl-1 ">
								<div class="check pt-2" align="center">
									<input type="checkbox" class="form-check-input" id="">
								</div>							
							</div>
							<div class="col-1 col-md-1 col-lg-1 col-xl-1 ">
								<img src="assets/img/profil3.jpg" class="img4">
							</div>
							<div class="col-2 col-md-2 col-lg-2 col-xl-2 list2">
								<div class="pt-3">
									<h6> WPAP</h6>
								</div>
							</div>
							<div class="col-2 col-md-2 col-lg-2 col-xl-2 list2">
								<div  align="center">
									<small>Ilustrasi</small>
									<h6>WPAP</h6>
								</div>							
							</div>
							<div class="col-2 col-md-2 col-lg-2 col-xl-2 list2">
								<div  align="center">
									<small>Tipe</small>
									<h6>Digital</h6>
								</div>							
							</div>						
							<div class="col-2 col-md-2 col-lg-2 col-xl-2 list2">
								<div  align="center">
									<small>Harga</small>
									<h6>Rp.100.000</h6>
								</div>							
							</div>
							<div class="col-1 col-md-1 col-lg-1 col-xl-1">
								<div class="pt-2" align="center">
									<a href="detail-rincian-pesanan.php" class="yellow">
										Rincian
									</a>
								</div>
							</div>
							<div class="col-1 col-md-1 col-lg-1 col-xl-1">
								<div class="" style="margin-top: 5px" align="center">
									<button type="button" class="btn btn-putih">
										<i class="fas fa-times" data-toggle="tooltip" data-placement="bottom" title="Hapus pesanan ini"></i>
									</button>
								</div>
							</div>
						</div>
					</div>					
				</div>

				<h6>Pesanan Koleksi</h6>

				<div id="cartList" class="order-active cartList bg-white">
					<div class="container"> 
						<div class="row scroll">
							<div class="col-1 col-md-1 col-lg-1 col-xl-1 ">
								<div class="check pt-2" align="center">
									<input type="checkbox" class="form-check-input" id="">
								</div>							
							</div>
							<div class="col-1 col-md-1 col-lg-1 col-xl-1 ">
								<img src="assets/img/profil3.jpg" class="img4">
							</div>
							<div class="col-2 col-md-2 col-lg-2 col-xl-2 list2">
								<div class="pt-3">
									<h6> WPAP</h6>
								</div>
							</div>
							<div class="col-2 col-md-2 col-lg-2 col-xl-2 list2">
								<div  align="center">
									<small>Ilustrasi</small>
									<h6>WPAP</h6>
								</div>							
							</div>
							<div class="col-2 col-md-2 col-lg-2 col-xl-2 list2">
								<div  align="center">
									<small>Tipe</small>
									<h6>Cetak Digital</h6>
								</div>							
							</div>						
							<div class="col-2 col-md-2 col-lg-2 col-xl-2 list2">
								<div  align="center">
									<small>Harga</small>
									<h6>Rp.100.000</h6>
								</div>							
							</div>
							<div class="col-1 col-md-1 col-lg-1 col-xl-1">
								<div class="pt-2" align="center">
									<a href="detail-rincian-pesanan.php" class="yellow">
										Rincian
									</a>
								</div>
							</div>
							<div class="col-1 col-md-1 col-lg-1 col-xl-1">
								<div class="" style="margin-top: 5px" align="center">
									<button type="button" class="btn btn-putih">
										<i class="fas fa-times" data-toggle="tooltip" data-placement="bottom" title="Hapus pesanan ini"></i>
									</button>
								</div>
							</div>
						</div>
					</div>					
				</div>

			</div>
			<div class="col-12 col-sm-12 col-md-4 col-lg-4">
				<div class="bg-white py-2 px-2">
					<h6 class="border-bottom b-600">Alamat Pengiriman</h6>
					<div class="form-group">
						<label>Provinsi</label>
						<div class="list3">
							<select class="form-control form-control-sm" id="">
								<option>Jawa tengah</option>
								<option>2</option>
								<option>3</option>
								<option>4</option>
								<option>5</option>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label>Kota/Kabupaten</label>
						<div class="list3">
							<select class="form-control form-control-sm" id="">
								<option>Surakarta</option>
								<option>2</option>
								<option>3</option>
								<option>4</option>
								<option>5</option>
							</select>
						</div>
					</div>
					<div class="row">
						<div class="col-12 col-sm-12 col-md-6">
							<div class="form-group">
								<label>Kecamatan</label>
								<div class="list3">
									<select class="form-control form-control-sm" id="">
										<option>Jebres</option>
										<option>2</option>
										<option>3</option>
										<option>4</option>
										<option>5</option>
									</select>
								</div>
							</div>
						</div>
						<div class="col-12 col-sm-12 col-md-6">
							<div class="form-group">
								<label>Kodepos</label>
								<div class="list3">
									<input type="email" class="form-control form-control-sm" id="" placeholder="Kodepos">
								</div>
							</div>
						</div>
					</div>
					<div class="form-group">
						<label>Alamat Lengkap</label>
						<div class="list3">
							<textarea class="form-control form-control-sm" id="" rows="3" placeholder="Masukkan Jalan, Nomor rumah, RT/RW"></textarea>
						</div>
					</div>
				</div>

				<div class="bg-white mt-3 py-2 px-2" >
					<div class="">
						<div class="">
							<h6 class="border-bottom"><b>Ringkasan Pembayaran</b></h6>
						</div>
						<div class="row">
							<div class="col-6 col-sm-6 col-md-6">
								<label class="f-14"> Total Harga </label>
								<label>Ongkos Kirim</label>
							</div>
							<div class="col-6 col-sm-6 col-md-6" align="right">
								<label class="f-14">Rp. 150.000</label>
								<label class="f-14">Rp. 15.000</label>								
							</div>
						</div>
						<div class="border-bottom">
							<div class="row">
								<div class="col-6 col-sm-6 col-md-6">
									<h6 class="f-14"> Total Pembayaran</h6>
								</div>
								<div class="col-6 col-sm-6 col-md-6 red2" align="right">
									<h6 class="f-14">Rp. 165.000</h6>
								</div>
							</div>
						</div>
						<div class="btn-detail mt-3">
							<a href="info-pembayaran.php" type="" class=" bg-blue btn-sm btn-block text-center">Bayar Sekarang</a>
						</div>
					</div>
				</div>

			</div>			
		</div>

	</div>

</div>
</div>



<?php
include "footer-dashboard.php";
?>
<?php
include "footer.php";
?>