<?php
include "header.php";
?>
<?php
include "header-dashboard.php";
?>

<div id="dashboard" class="dashboard bg-grey">
	<div class="container">
		<div class="bg-white py-5 px-5">
			<div class="" align="center">
				<h6>Nominal saldo yang dicairkan</h6>
				<div class="border-bottom border-top py-3 mb-3">
					<h4 class="pink b-600">Rp. 2.500.000</h4>
				</div>
				<div class="icon-bank">
					<h6 class="f-14 mb-3">Ditransfer ke: <a href="#" data-toggle="modal" data-target="#exampleModal">Masukkan alamat rekening</a></h6>
					<img src="assets/img/icon/bca.png">
					<h6 class="mt-2">09897883927332</h6>
					<small>A/N</small>
					<h6>Gilang bogy</h6>
					<div class="btn-withdraw mb-5">
						<button type="submit" class="btn bg-biru btn-sm">Cairkan sekarang</button>	
					</div>
				</div>
				<small>* Permintaan pencairan dana akan membutuhkanwaktu maksimal 2 x 24 jam (Hari kerja)</small>
			</div>
		</div>
	</div>
</div>


<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Transfer ke :</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form>
					<div class="form-group">
						<label>Pilih Bank</label>
						<div class="list3">
							<select class="form-control" id="">
								<option>Bank BCA</option>
								<option>Bank Mandiri</option>
								<option>Bank BRI</option>
								<option>Bank BNI</option>
							</select>
						</div>			   
					</div>
					<div class="row">
						<div class="col-12 col-sm-6 col-md-6">
							<div class="form-group">
								<label>Atas Nama</label>
								<div class="list3">
									<input type="text" class="form-control" id="" aria-describedby="emailHelp" placeholder="Masukkan nama">
								</div>
							</div>
						</div>
						<div class="col-12 col-sm-6 col-md-6">
							<div class="form-group">
								<label>No Rekening</label>
								<div class="list3">
									<input type="number" min="0" class="form-control" id="" aria-describedby="emailHelp" placeholder="Masukkan nomor rekening">
								</div>
							</div>
						</div>
					</div>
					<div class="btn-withdraw" align="center">
						<button type="submit" class="btn btn-biru btn-sm">Simpan</button>
					</div>
					
				</form>
			</div>
		</div>
	</div>
</div>




<?php
include "footer-dashboard.php";
?>
<?php
include "footer.php";
?>