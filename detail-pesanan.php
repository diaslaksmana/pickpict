<?php
include "header.php";
?>
<?php
include "header2.php";
?>

<div id="detail-product" class="bg-color-grey">
	<div class="container">
		<div class="upload bg-white">
			<div class="row ">
				<div class="col-4 col-sm-4 col-md-4 list5" align="right">
					<div class="progres step1">
						<h6> Pesanan</h6>
						<i class="fas fa-check-circle"></i>
					</div>
				</div>
				<div class="col-4 col-sm-4 col-md-4 list5" >
					<div class="progres " align="center">
						<h6>Upload</h6>
						<i class="fas fa-check-circle"></i>
					</div>
				</div>
				<div class="col-4 col-sm-4 col-md-4 list5" align="left">
					<div class="progres step3 ">
						<h6><b>Detail </b></h6>
						<i class="fas fa-dot-circle"></i>
					</div>
				</div>
			</div>

			<div class="row mt-5">
				<div class="col-12 col-sm-12 col-md-4 col-lg-4">
					<div class="img-ilustrasi">
						<img src="assets/img/profil3.jpg">
					</div>
				</div>
				<div class="col-12 col-sm-12 col-md-8 col-lg-8">
					<h5 class="b-600">WPAP</h5>
					<div class="list-bottom mb-2">
						<h6 class="b-600 f-14">Yang Kamu Dapat</h6>
					</div>
					<div class="row f-12">
						<div class="col-6 col-sm-3 col-md-3">
							<h6>Jumlah subjek</h6>
							<h6>Warna</h6>
							<h6>Proporsi</h6>
							<h6>Jenis File </h6>
						</div>
						<div class="col-6 col-sm-3 col-md-3">
							<h6>: 1</h6>
							<h6>: Full color</h6>
							<h6>: Close-up</h6>
							<h6>: JPG</h6>
						</div>
					</div>

					<div class="list-bottom mb-2">
						<h6 class="b-600 f-14">Tambah Fitur</h6>
					</div>
					<table class="table-order" width="70%;">
						<thead>
							<tr>
								<th scope="col">Nama Fitur</th>
								<th scope="col"></th>
								<th scope="col"></th>
								<th scope="col" style="text-align: center;">Harga</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td colspan="2">Tambah subjek</td>
								<td colspan="2" align="right">Rp 100.000</td>
							</tr>
							<tr>
								<td colspan="2">File Vektor</td>
								<td colspan="2" align="right">Rp 150.000</td>
							</tr>
							<tr>
								<td colspan="2">Hak Cipts</td>
								<td colspan="2" align="right">-</td>
							</tr>
							<tr>
								<th scope="col">Cetak Produk</th>
								<th scope="col">Ukuran</th>
								<th scope="col">Qty</th>
								<th scope="col"></th>
							</tr>
							<tr>
								<td>Bantal</td>
								<td>40x40</td>
								<td align="center">1</td>
								<td align="right">Rp 150.000</td>
							</tr>
							<tr>
								<td colspan="3"align="right" style="padding-right: 25px;font-weight: bold;">
									Ongkir
								</td>
								<td align="right">Rp 25.000</td>
							</tr>
							<tr>
								<th scope="col">Total Pembayaran</th>
								<th scope="col"></th>
								<th scope="col"></th>
								<th scope="col" style="text-align: right;">
									Rp 425.000
								</th>
							</tr>
						</tbody>
					</table>

					<div class="list-bottom mt-4 mb-2">
						<h6 class="b-600 f-14">Waktu Pengerjaan</h6>
					</div>	
					<h6 class="f-12">4 Hari</h6>

					<div class="info-address mt-3">
						<div class="row mb-3 btn-modal-edit">
							<div class="col-6 col-sm-6 col-md-6 f-12" align="left">
								<h6><b>Alamat Pengiriman</b></h6>
							</div>
							<div class="col-6 col-sm-6 col-md-6 " align="right">
								<a class="" data-toggle="modal" data-target="#ubah_alamat_pengiriman">
									<i class="far fa-edit"></i> Edit alamat
								</a>

							</div>
						</div>							
						<div class="row">
							<div class="col-4 col-sm-6 col-md-3 f-12">
								<h6>Provinsi</h6>
								<h6>Kota</h6>
								<h6>Kecamatan</h6>
								<h6>Kodepos</h6>
								<h6>Alamat</h6>
							</div>
							<div class="col-8 col-sm-6 col-md-9 f-12">
								<h6>: Jawa Tengah</h6>
								<h6>: Kota Surakarta</h6>
								<h6>: Jebres</h6>
								<h6>: 78709</h6>
								<h6>: Jl. Kartika nomer 10 RT 11/ RW 2</h6>
							</div>
						</div>
					</div>

					<div class="btn-my-order my-4">
						<a class="btn mr-2 btn-sm" href="info-pembayaran.php" role="button">Beli</a>

						<button class="btn  btn-sm" type="submit" role="button"><i class="fas fa-shopping-bag"></i> Tambah ke keranjang</button>
					</div>

				</div>
			</div>

		</div>
	</div>
</div>

<div class="modal fade" id="ubah_alamat_pengiriman" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Ganti Alamat Pengiriman</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form>
					<div class="form-group">
						<label>Provinsi</label>
						<div class="list3">
							<select class="form-control form-control-sm" id="">
								<option>Jawa tengah</option>
								<option>2</option>
								<option>3</option>
								<option>4</option>
								<option>5</option>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label>Kota/Kabupaten</label>
						<div class="list3">
							<select class="form-control form-control-sm" id="">
								<option>Surakarta</option>
								<option>2</option>
								<option>3</option>
								<option>4</option>
								<option>5</option>
							</select>
						</div>
					</div>
					<div class="row">
						<div class="col-12 col-sm-12 col-md-6">
							<div class="form-group">
								<label>Kecamatan</label>
								<div class="list3">
									<select class="form-control form-control-sm" id="">
										<option>Jebres</option>
										<option>2</option>
										<option>3</option>
										<option>4</option>
										<option>5</option>
									</select>
								</div>
							</div>
						</div>
						<div class="col-12 col-sm-12 col-md-6">
							<div class="form-group">
								<label>Kodepos</label>
								<div class="list3">
									<input type="email" class="form-control form-control-sm" id="" placeholder="Kodepos">
								</div>
							</div>
						</div>
					</div>
					<div class="form-group">
						<label>Alamat Lengkap</label>
						<div class="list3">
							<textarea class="form-control form-control-sm" id="" rows="2" placeholder="Masukkan Jalan, Nomor rumah, RT/RW"></textarea>
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Batal</button>
				<button type="submit" class="btn  btn-success btn-sm">Simpan</button>
			</div>
		</div>
	</div>
</div>

<?php
include "footer2.php";
?>
<?php
include "footer.php";
?>