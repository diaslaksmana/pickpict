<?php
include "header.php";
?>
<?php
include "header2.php";
?>

<div id="detail-product" class="bg-color-grey">
	<div class="container">
		<div class="detail-product bg-white">
			<div class="row">
				<div class="col-12 col-sm-12 col-md-4 col-lg-4">
					<div class="img-display">
						<div class="">
							<img src="assets/img/b1.jpg"  >
						</div>
						<div class="row mt-2 mb-4">
							<div class="col-4 col-sm-4 col-md-4 col-lg-4">
								<img src="assets/img/wpap.jpg" >
							</div>
							<div class="col-4 col-sm-4 col-md-4 col-lg-4">
								<img src="assets/img/w1.png" >
							</div>
							<div class="col-4 col-sm-4 col-md-4 col-lg-4">
								<img src="assets/img/d3.jpg">
							</div>
						</div>
					</div>
				</div>
				<div class="col-12 col-sm-12 col-md-8 col-lg-8">
					<div class="row title-custom">
						<div class="col-5 col-sm-4 col-md-4">
							<div class="form-group">
								<label for="">Pilih Ilustrasi</label>
								<select class="form-control form-control-sm" id="">
									<option>WPAP</option>
									<option>Vektor</option>
									<option>Pop Art</option>
									<option>Karttun</option>
								</select>
							</div>
						</div>
						<div class="col-7 col-sm-8 col-md-8" align="right">
							<h6 class="grey">
								Durasi pengerjaan : 4 Hari
							</h6>
						</div>
					</div>
					<div class="detail-info-creator">
						<div class="rating3">
							<h6>
								<i class="fas fa-star"></i>
								<i class="fas fa-star"></i>
								<i class="fas fa-star"></i>
								<i class="fas fa-star"></i>
								<i class="fas fa-star"></i>
								<span>
									(50 Ulasan)
								</span>
							</h6>
						</div>
						<p class="grey">
							<i>Tersedia dalam : Pigura, Bantal, Case HP</i>
						</p>
						<h6 class="grey type-ilustrasi">
							Tipe : <span><i> WPAP </i></span>
						</h6>
						<h6 class="creatorBy mt-3">
							Creator by <a href="">John Dae</a>
						</h6>
						<h3 class="mt-3">
							<b>Rp. 100.000</b>
						</h3>
						<div class="title4 mt-3 mb-2">
							<h6><b>Deskripsi</b></h6>
						</div>
						<p>
							Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electroni
						</p>
						<div class="title4 mt-3 mb-2">
							<h6><b>Yang kamu dapatkan</b></h6>
						</div>
						<div class="row f-12">
							<div class="col-6 col-sm-3 col-md-3">
								<h6>Jumlah subjek</h6>
								<h6>Warna</h6>
								<h6>Proporsi</h6>
								<h6>Jenis File </h6>
							</div>
							<div class="col-6 col-sm-3 col-md-3">
								<h6>: 1</h6>
								<h6>: Full color</h6>
								<h6>: Close-up</h6>
								<h6>: JPG</h6>
							</div>
						</div>
						<div class="title4 mt-3 mb-2">
							<h6><b>Tambah Fitur</b></h6>
						</div>
						<div class="table-responsive width-table">
							<table class="">								
								<thead>
									<tr>
										<th scope="col ">Nama Fitur</th>
										<th scope="col">Tambahan Waktu</th>
										<th scope="col">Tambahan Harga</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>Jumlah Subjek</td>
										<td align="center">1 Hari</td>
										<td align="right">	Rp 100.000</td>
									</tr>
									<tr>
										<td>Hak cipta</td>
										<td align="center">-</td>
										<td align="right">Rp 500.000</td>
									</tr>
									<tr>
										<td>File vektor</td>
										<td align="center">-</td>
										<td align="right">	Rp 250.000</td>
									</tr>
								</tbody>													
							</table>
						</div>
						<div class="btn-order mt-4">
							<a href="pesanan-anda.php" class="bg-yellow white">
								Selanjutnya
							</a>
						</div>
					</div>
				</div>
			</div>

			<div class="coment-product my-3">
				<div class="mb-4" align="left">
					<h6><b>Ulasan Produk</b></h6>
				</div>

				<div class="owl-carousel owl-theme mb-3">
					<div class="item">
						<div class="box">
							<div class="row">
								<div class="col-3 col-sm-3 col-md-3">
									<div class="avatar2">
										<img src="assets/img/avatar.png">
									</div>
								</div>
								<div class="col-9 col-sm-9 col-md-9">
									<h6>Chris Evans</h6>
									<small>23 Maret 2019</small>
								</div>
							</div>
							<div class="rating mt-3" align="center">
								<label>
									<i class="fas fa-star"></i>
									<i class="fas fa-star"></i>
									<i class="fas fa-star"></i>
									<i class="fas fa-star"></i>
									<i class="fas fa-star"></i>
								</label>
							</div>
							<p align="center">
								Bagus sekali pengerjaannya, memuaskan, top !
							</p>
						</div>
					</div>
					<div class="item">
						<div class="box">
							<div class="row">
								<div class="col-3 col-sm-3 col-md-3">
									<div class="avatar2">
										<img src="assets/img/avatar.png">
									</div>
								</div>
								<div class="col-9 col-sm-9 col-md-9">
									<h6>Robert Downey</h6>
									<small>23 Maret 2019</small>
								</div>
							</div>
							<div class="rating mt-3" align="center">
								<label>
									<i class="fas fa-star"></i>
									<i class="fas fa-star"></i>
									<i class="fas fa-star"></i>
									<i class="fas fa-star"></i>
									<i class="fas fa-star"></i>
								</label>
							</div>
							<p align="center">
								Bagus sekali pengerjaannya, memuaskan, top !
							</p>
						</div>
					</div>
					<div class="item">
						<div class="box">
							<div class="row">
								<div class="col-3 col-sm-3 col-md-3">
									<div class="avatar2">
										<img src="assets/img/avatar.png">
									</div>
								</div>
								<div class="col-9 col-sm-9 col-md-9">
									<h6>Tcakala</h6>
									<small>23 Maret 2019</small>
								</div>
							</div>
							<div class="rating mt-3" align="center">
								<label>
									<i class="fas fa-star"></i>
									<i class="fas fa-star"></i>
									<i class="fas fa-star"></i>
									<i class="fas fa-star"></i>
									<i class="fas fa-star"></i>
								</label>
							</div>
							<p align="center">
								Bagus sekali pengerjaannya, memuaskan, top !
							</p>
						</div>
					</div>
					<div class="item">
						<div class="box">
							<div class="row">
								<div class="col-3 col-sm-3 col-md-3">
									<div class="avatar2">
										<img src="assets/img/avatar.png">
									</div>
								</div>
								<div class="col-9 col-sm-9 col-md-9">
									<h6>Chris Evans</h6>
									<small>23 Maret 2019</small>
								</div>
							</div>
							<div class="rating mt-3" align="center">
								<label>
									<i class="fas fa-star"></i>
									<i class="fas fa-star"></i>
									<i class="fas fa-star"></i>
									<i class="fas fa-star"></i>
									<i class="fas fa-star"></i>
								</label>
							</div>
							<p align="center">
								Bagus sekali pengerjaannya, memuaskan, top !
							</p>
						</div>
					</div>
					<div class="item">
						<div class="box">
							<div class="row">
								<div class="col-3 col-sm-3 col-md-3">
									<div class="avatar2">
										<img src="assets/img/avatar.png">
									</div>
								</div>
								<div class="col-9 col-sm-9 col-md-9">
									<h6>Chris Evans</h6>
									<small>23 Maret 2019</small>
								</div>
							</div>
							<div class="rating mt-3" align="center">
								<label>
									<i class="fas fa-star"></i>
									<i class="fas fa-star"></i>
									<i class="fas fa-star"></i>
									<i class="fas fa-star"></i>
									<i class="fas fa-star"></i>
								</label>
							</div>
							<p align="center">
								Bagus sekali pengerjaannya, memuaskan, top !
							</p>
						</div>
					</div>
				</div>

			</div>

		</div>
	</div>
</div>

<?php
include "footer2.php";
?>
<?php
include "footer.php";
?>