<?php
include "header.php";
?>
<?php
include "header-dashboard.php";
?>

<div id="dashboard" class="dashboard bg-grey">
	<div class="container">
		<div class="row">
			<div class="col-12 col-sm-12 col-md-4 col-lg-3 col-xl-3">
				<div class="nav-pills-blue mb-5">
					<div class="nav flex-column " id="v-pills-tab" role="tablist" aria-orientation="vertical">
						<a class="nav-link active" id="v-pills-tagihan-tab" data-toggle="pill" href="#v-pills-tagihan" role="tab" aria-controls="v-pills-tagihan" aria-selected="true">
							<i class="fas fa-clipboard-list"></i> Tagihan
						</a>
						<a class="nav-link" id="v-pills-pembayaran-sukses-tab" data-toggle="pill" href="#v-pills-pembayaran-sukses" role="tab" aria-controls="v-pills-pembayaran-sukses" aria-selected="false">
							<i class="fas fa-check-double"></i> Pembayaran Sukses
						</a>
						<a class="nav-link" id="v-pills-riwayat-pembelian-tab" data-toggle="pill" href="#v-pills-riwayat-pembelian" role="tab" aria-controls="v-pills-riwayat-pembelian" aria-selected="false">
							<i class="fas fa-history"></i> Riwayat Pembelian
						</a>
					</div>
				</div>				
			</div>
			<div class="col-12 col-sm-12 col-md-8 col-lg-9 col-xl-9">
				<div class="tab-content" id="v-pills-tabContent">
					<div class="tab-pane fade show active" id="v-pills-tagihan" role="tabpanel" aria-labelledby="v-pills-tagihan-tab">

						<div class="bg-white b-r-5 py-3 px-2">
							<h6>Silahkan Upload bukti transfer anda</h6>
							<div class="table-responsive">
								<table class="table table-striped font-table">
									<thead>
										<tr>
											<th scope="col">ID Transaksi</th>
											<th scope="col">Total</th>
											<th scope="col">Batas Pembayaran</th>
											<th scope="col">Status</th>
											<th scope="col">Rincian</th>
											<th scope="col"></th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<th scope="row">PI-089-990789</th>
											<td>RP. 250.000,00</td>
											<td>15 Januari 2018, pukul 18.00 WIB</td>
											<td class="yellow">
												Baru
											</td>
											<td class="btn-detail-order f-12">
												<a class=" bg-blue btn-sm" href="detail-status-pembayaran.php" role="button">
													Detail
												</a>
											</td>
											<td class="btn-detail-order f-12" align="center">
												<a class=" bg-yellow btn-sm" role="button" data-toggle="collapse" href="#upload_pembayaran" role="button" aria-expanded="false" aria-controls="collapseExample">
													Upload 
												</a>
											</td>
										</tr>
										<tr>
											<th scope="row">PI-089-990789</th>
											<td>RP. 250.000,00</td>
											<td>15 Januari 2018, pukul 18.00 WIB</td>
											<td class="yellow">
												Menunggu Konfirmasi
											</td>
											<td class="btn-detail-order f-12">
												<a class=" bg-blue btn-sm" href="detail-status-pembayaran.php" role="button">
													Detail
												</a>
											</td>
											<td class="btn-detail-order f-12" align="center">
												<a class=" bg-yellow btn-sm" role="button" data-toggle="collapse" href="#upload_pembayaran" role="button" aria-expanded="false" aria-controls="collapseExample">
													Upload 
												</a>
											</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>

					</div>
					<div class="tab-pane fade" id="v-pills-pembayaran-sukses" role="tabpanel" aria-labelledby="v-pills-pembayaran-sukses-tab">

						<div class="bg-white py-3 px-2 b-r-5">
							<div class="row mb-3">
								<div class="col-12 col-sm-12 col-md-8">
									<h6 class="pt-1">Selamat Pembayaran anda sukses !</h6>
								</div>
								<div class="col-12 col-sm-12 col-md-4" align="right">
									<div class="">
										<div class="input-group">           
											<input class="form-control form-control-sm" type="text" placeholder="Cari ID Order"  aria-describedby="sizing-addon2" value/>
										</div>
									</div>
								</div>
							</div>
							<div class="table-responsive">
								<table class="table table-striped font-table">
									<thead>
										<tr>
											<th scope="col">ID Transaksi </th>
											<th scope="col">ID Order</th>
											<th scope="col">Tgl Pembayaran</th>
											<th scope="col">Total</th>
											<th scope="col">Status</th>
											<th scope="col" style="text-align: center">Rincian</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<th scope="row">PI-0979-8798-09</th>
											<td>PI-0979-8798-01</td>
											<td>30 Desember 2018</td>
											<td>RP. 250.000,00</td>
											<td class="blue">
												<i class="fas fa-check-circle"></i> Dibayar
											</td>
											<td class="btn-detail" align="center">
												<a class=" bg-blue btn-sm" href="detail-status-pembayaran.php" role="button">
													Detail
												</a>
											</td>
										</tr>

									</tbody>
								</table>
							</div>
						</div>		

					</div>
					<div class="tab-pane fade" id="v-pills-riwayat-pembelian" role="tabpanel" aria-labelledby="v-pills-riwayat-pembelian-tab">

						<div class="bg-white py-3 px-2 b-r-5">
							<div class="row mb-3">
								<div class="col-12 col-sm-12 col-md-8">
									<h6 class="pt-1">Riwayat Pembelian</h6>
								</div>
								<div class="col-12 col-sm-12 col-md-4" align="right">
									<div class="">
										<div class="input-group">           
											<input class="form-control form-control-sm" type="text" placeholder="Cari ID Order"  aria-describedby="sizing-addon2" value/>
										</div>
									</div>
								</div>
							</div>
							<div class="table-responsive">
								<table class="table table-striped font-table">
									<thead>
										<tr>
											<th scope="col">ID Transaksi </th>
											<th scope="col">ID Order</th>
											<th scope="col">Tgl Pembayaran</th>
											<th scope="col">Total</th>
											<th scope="col">Status</th>
											<th scope="col" style="text-align: center">Rincian</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<th scope="row">PI-0979-8798-09</th>
											<td>PI-0979-8798-01</td>
											<td>30 Desember 2018</td>
											<td>RP. 250.000,00</td>
											<td class="blue">
												<i class="fas fa-dolly-flatbed"></i> 
												Terkirim
											</td>
											<td class="btn-detail " align="center">
												<a class=" bg-pink btn-sm" href="detail-status-pembayaran.php" role="button">
													Detail
												</a>
											</td>
										</tr>

									</tbody>
								</table>
							</div>
						</div>

					</div>
				</div>

				<!-- upload bukti pembayaran-->

				<div class="collapse tagihan_font" id="upload_pembayaran" style="margin-top: 25px">
					<div class="card card-body upload-font upload-payment">
						<h6 class="border-bottom">Upload Bukti Pembayaran</h6>
						<form>
							<div class="form-group">
								<label>Pilih Bank</label>
								<div class="list3">
									<select class="form-control" id="">
										<option>Bank BCA</option>
										<option>Bank Mandiri</option>
										<option>Bank BRI</option>
										<option>Bank BNI</option>
									</select>
								</div>			   
							</div>
							<div class="row">
								<div class="col-12 col-sm-6 col-md-6">
									<div class="form-group">
										<label>Atas Nama</label>
										<div class="list3">
											<input type="text" class="form-control" id="" aria-describedby="emailHelp" placeholder="Masukkan nama">
										</div>
									</div>
								</div>
								<div class="col-12 col-sm-6 col-md-6">
									<div class="form-group">
										<label>No Rekening</label>
										<div class="list3">
											<input type="number" min="0" class="form-control" id="" aria-describedby="emailHelp" placeholder="Masukkan nomor rekening">
										</div>
									</div>
								</div>
							</div>
							<div class="form-group">
								<label for="exampleFormControlFile1">Upload Bukti Transfer</label>
								<input type="file" class="form-control-file" id="">
							</div>

							<div class="btn-blue" align="center">
								<button type="submit" class="btn bg-blue btn-sm w-100">Kirim</button>
							</div>

						</form>
					</div>
				</div>

			</div>
		</div>
	</div>
</div>




<?php
include "footer-dashboard.php";
?>
<?php
include "footer.php";
?>