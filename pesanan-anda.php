<?php
include "header.php";
?>
<?php
include "header2.php";
?>

<div id="detail-product" class="bg-color-grey">
	<div class="container">
		<div class="detail-order bg-white">
			<div class="row ">
				<div class="col-4 col-sm-4 col-md-4 list5" align="right">
					<div class="progres step1 ">
						<h6><b> Pesanan </b></h6>
						<i class="fas fa-dot-circle"></i>
					</div>
				</div>
				<div class="col-4 col-sm-4 col-md-4 list5" >
					<div class="progres " align="center">
						<h6>Upload</h6>
						<i class="fas fa-circle"></i>
					</div>
				</div>
				<div class="col-4 col-sm-4 col-md-4 list5" align="left">
					<div class="progres step3">
						<h6>Detail</h6>
						<i class="fas fa-circle"></i>
					</div>
				</div>
			</div>

			<div class="row mt-5">
				<div class="col-12 col-sm-12 col-md-5 col-lg-3">
					<div class="img-ilustrasi">
						<h5 class="b-600">Ilustrasi : WPAP</h5>
						<img src="assets/img/wpap2.jpg">
					</div>
				</div>
				<div class="col-12 col-sm-12 col-md-7 col-lg-5">
					<div class="list-bottom mb-2">
						<h5 class="b-600 f-14">Yang Kamu Dapat</h5>
					</div>
					<div class="row f-12">
						<div class="col-6 col-sm-3 col-md-3">
							<h6>Jumlah subjek</h6>
							<h6>Warna</h6>
							<h6>Proporsi</h6>
							<h6>Jenis File </h6>
						</div>
						<div class="col-6 col-sm-3 col-md-3">
							<h6>: 1</h6>
							<h6>: Full color</h6>
							<h6>: Close-up</h6>
							<h6>: JPG</h6>
						</div>
					</div>

					<div class="list-bottom mb-2">
						<h5 class="f-14"><b>Tambah Fitur</b></h5>
					</div>
					<div class="row add-fitur">
						<div class="col-6 col-sm-6 col-md-6">
							<h6 class="b-600 f-14">Nama Fitur</h6>
							<div class="check-container">
								<input type="checkbox" id="one"></input>
								<label for="one"></label>
								<span class="tag">Tambah Subjek</span>
							</div><br>
							<div class="check-container">
								<input type="checkbox" id="two"></input>
								<label for="two"></label>
								<span class="tag">Hak Cipta</span>
							</div><br>
							<div class="check-container">
								<input type="checkbox" id="three"></input>
								<label for="three"></label>
								<span class="tag">File Vektor</span>
							</div>
						</div>
						<div class="col-6 col-sm-6 col-md-6 ">
							<h6 class="b-600 f-14">Tambah Harga</h6>
							<h5>Rp 100.000</h5>
							<h5>Rp 500.000</h5> 
							<h5>Rp 250.000</h5>
						</div>
					</div>
					<div class="row p-produk mt-3">
						<div class="col-12 col-sm-12 col-md-8 col-lg-7">
							<p>Mau sekalian cetak ke Produk ?</p>
						</div>
						<div class="col-12 col-sm-12 col-md-4 col-lg-4">
							<input type="radio" id="radio01" name="radio" value="ya" onclick="show2();"  />
							<label for="radio01"><span></span>Ya</label>

							<input type="radio" id="radio02" name="radio" value="tidak" onclick="show1();"/>
							<label for="radio02"><span></span>Tidak</label>
						</div>
					</div>

					<div id="div1" class="hide">
						<div class="produk-print">
							<h6>Pilih detail produk</h6>
							<div class="row">
								<div class="col-12 col-sm-4 col-md-4">
									<div class="form-group">
										<label>Produk</label>
										<div class="list3">
											<select class="form-control form-control-sm" id="">
												<option>Kaos</option>
												<option>MUG</option>
												<option>Bantal</option>
												<option>Case</option>
											</select>
										</div>
									</div>
								</div>
								<div class="col-12 col-sm-3 col-md-3">
									<div class="form-group">
										<label >Qty</label>
										<div class="list3">
											<input type="email" class="form-control form-control-sm" min="0" id="" placeholder="Qty">
										</div>
									</div>
								</div>
							</div>

							<div class="size-produk">
								<div class="my-3">
									<h6><b>Pilih Ukuran</b></h6>
								</div>

								<div class="row mb-5">
									<div class="col-6 col-sm-6 col-md-3 " align="center">
										<img src="assets/img/s2.jpg" >
										<h5>L</h5>
										<input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios1" value="option1" checked>
									</div>
									<div class="col-6 col-sm-6 col-md-3" align="center">
										<img src="assets/img/s6.jpg" >
										<h5>M</h5>
										<input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios2" value="option2">
									</div>
									<div class="col-6 col-sm-6 col-md-3">

									</div>
								</div>
							</div>

						</div>
						<div class="">
							<h6>Alamat Pengiriman</h6>
							<div class="form-group">
								<label>Provinsi</label>
								<div class="list3">
									<select class="form-control form-control-sm" id="">
										<option>Jawa tengah</option>
										<option>2</option>
										<option>3</option>
										<option>4</option>
										<option>5</option>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label>Kota/Kabupaten</label>
								<div class="list3">
									<select class="form-control form-control-sm" id="">
										<option>Surakarta</option>
										<option>2</option>
										<option>3</option>
										<option>4</option>
										<option>5</option>
									</select>
								</div>
							</div>
							<div class="row">
								<div class="col-12 col-sm-12 col-md-6">
									<div class="form-group">
										<label>Kecamatan</label>
										<div class="list3">
											<select class="form-control form-control-sm" id="">
												<option>Jebres</option>
												<option>2</option>
												<option>3</option>
												<option>4</option>
												<option>5</option>
											</select>
										</div>
									</div>
								</div>
								<div class="col-12 col-sm-12 col-md-6">
									<div class="form-group">
										<label>Kodepos</label>
										<div class="list3">
											<input type="email" class="form-control form-control-sm" id="" placeholder="Kodepos">
										</div>
									</div>
								</div>
							</div>
							<div class="form-group">
								<label>Alamat Lengkap</label>
								<div class="list3">
									<textarea class="form-control form-control-sm" id="" rows="2" placeholder="Masukkan Jalan, Nomor rumah, RT/RW"></textarea>
								</div>
							</div>
						</div>
					</div>
					<div class="catatan_pembeli">
						<div class="form-group">
							<label class="f-14">Catatan Pesanan</label>
							<div class="list3">
								<textarea class="form-control form-control-sm" id="" placeholder="Tuliskan catatan kamu..." rows="3"></textarea>
							</div>

						</div>
					</div>
					
					<div class="btn-order my-4">
						<a href="upload.php" class="bg-yellow white">
							Selanjutnya
						</a>
					</div>

				</div>
				<div class="col-12 col-sm-12 col-md-6 col-lg-4">
					<div class="summary">
						<h6 class="b-600">Ringkasan Pesanan</h6>
						<div class="row">
							<div class="col-6 col-sm-6 col-md-6">
								<h5>Waktu Pengerjaan </h5>
							</div>
							<div class="col-6 col-sm-6 col-md-6" align="right">
								<h5> 4 Hari</h5>
							</div>
						</div>
						<div class="row">
							<div class="col-6 col-sm-6 col-md-6">
								<h5>Harga </h5>
							</div>
							<div class="col-6 col-sm-6 col-md-6" align="right">
								<h5> Rp. 150.000</h5>
							</div>
						</div>
						<div class="row">
							<div class="col-6 col-sm-6 col-md-6">
								<h5>Ongkir </h5>
							</div>
							<div class="col-6 col-sm-6 col-md-6" align="right">
								<h5> -</h5>
							</div>
						</div>
						<div class="row" >
							<div class="col-6 col-sm-6 col-md-6">
								<h5>Total Pembayaran </h5>
							</div>
							<div class="col-6 col-sm-6 col-md-6" align="right">
								<h5><b> Rp 150.000 </b></h5>
							</div>
						</div>
					</div>	
				</div>
			</div>
		</div>		
	</div>
</div>


<?php
include "footer2.php";
?>
<?php
include "footer.php";
?>