<?php
include "header.php";
?>
<?php
include "header-dashboard.php";
?>

<div id="dashboard" class="dashboard bg-grey">
	<div class="container">
		<div class="row">
			<div class="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-2">
				<div class="nav-pills-blue mb-5">
					<div class="nav flex-column " id="v-pills-tab" role="tablist" aria-orientation="vertical">
						<a class="nav-link active" id="v-pills-ilustrasi-favorite-tab" data-toggle="pill" href="#v-pills-ilustrasi-favorite" role="tab" aria-controls="v-pills-ilustrasi-favorite" aria-selected="true">
							<i class="fas fa-pencil-alt"></i> Ilustrasi
						</a>
						<a class="nav-link" id="v-pills-koleksi-favorit-tab" data-toggle="pill" href="#v-pills-koleksi-favorit" role="tab" aria-controls="v-pills-koleksi-favorit" aria-selected="false">
							<i class="fas fa-gift"></i> Koleksi
						</a>
						<a class="nav-link" id="v-pills-creator-favorit-tab" data-toggle="pill" href="#v-pills-creator-favorit" role="tab" aria-controls="v-pills-creator-favorit" aria-selected="false">
							<i class="fas fa-users"></i> Creator
						</a>
					</div>
				</div>				
			</div>
			<div class="col-12 col-sm-12 col-md-9 col-lg-9 col-xl-10">
				<div class="tab-content" id="v-pills-tabContent">
					<div class="tab-pane fade show active" id="v-pills-ilustrasi-favorite" role="tabpanel" aria-labelledby="v-pills-ilustrasi-favorite-tab">

						<div class="row mb-4">
							<div class="col-6 col-sm-6 col-md-6 col-lg-6 col-xl-6" align="left">
								<h6 class="b-600">Ilustrasi Favorit</h6>
							</div>
							<div class="col-6 col-sm-6 col-md-6 col-lg-6 col-xl-6" align="right">
								<div class="btn-yellow">
									<button type="submit" class="btn btn-sm">Hapus Semua </button>
								</div>
							</div>
						</div>
						<div class="row mt-4">
							<div class="col-12 col-sm-6 col-md-6 col-lg-3">
								<div class="postcard">					
									<div class="cover100">
										<img src="assets/img/kaos.jpg">
									</div>
									<div class="postcard-body">
										<a href="detail-produk.php">
											<h5>John Lennon</h5>
											<h6>WPAP</h6>
											<small>by John Dae</small>
										</a>						
									</div>
								</div>
							</div>
							<div class="col-12 col-sm-6 col-md-6 col-lg-3">
								<div class="postcard">					
									<div class="cover100">
										<img src="assets/img/mug.jpeg">
									</div>
									<div class="postcard-body">
										<a href="">
											<h5>Taylor Swift</h5>
											<h6>Mug</h6>
											<small>by John Dae</small>
										</a>						
									</div>
								</div>
							</div>
							<div class="col-12 col-sm-6 col-md-6 col-lg-3">
								<div class="postcard">					
									<div class="cover100">
										<img src="assets/img/bantal.jpg">
									</div>
									<div class="postcard-body">
										<a href="">
											<h5>Women</h5>
											<h6>Bantal</h6>
											<small>by John Dae</small>
										</a>						
									</div>
								</div>
							</div>
							<div class="col-12 col-sm-6 col-md-6 col-lg-3">
								<div class="postcard">					
									<div class="cover100">
										<img src="assets/img/casehp.jpg">
									</div>
									<div class="postcard-body">
										<a href="">
											<h5>OASIS</h5>
											<h6>WPAP</h6>
											<small>by John Dae</small>
										</a>						
									</div>
								</div>
							</div>
						</div>

					</div>
					<div class="tab-pane fade" id="v-pills-koleksi-favorit" role="tabpanel" aria-labelledby="v-pills-koleksi-favorit-tab">

						<div class="row mb-4">
							<div class="col-6 col-sm-6 col-md-6 col-lg-6 col-xl-6" align="left">
								<h6 class="b-600">Koleksi Favorit</h6>
							</div>
							<div class="col-6 col-sm-6 col-md-6 col-lg-6 col-xl-6" align="right">
								<div class="btn-yellow">
									<button type="submit" class="btn btn-sm">Hapus Semua </button>
								</div>
							</div>
						</div>
						<div class="row mt-4">
							<div class="col-12 col-sm-6 col-md-6 col-lg-3">
								<div class="postcard">					
									<div class="cover100">
										<span>
											Rp 120.000
										</span>
										<img src="assets/img/kaos.jpg">
									</div>
									<div class="postcard-body">
										<a href="detail-produk.php">
											<h5>John Lennon</h5>
											<h6>WPAP</h6>
											<small>by John Dae</small>
										</a>						
									</div>
									<div class="postcard-footer">
										<label>
											<i class="far fa-eye"></i> 100
										</label>
										<label>
											<i class="fas fa-heart"></i> 50
										</label>
										<label>
											<i class="fab fa-twitch"></i> 80
										</label>
									</div>
								</div>
							</div>
							<div class="col-12 col-sm-6 col-md-6 col-lg-3">
								<div class="postcard">					
									<div class="cover100">
										<span>
											Rp 50.000
										</span>
										<img src="assets/img/mug.jpeg">
									</div>
									<div class="postcard-body">
										<a href="">
											<h5>Taylor Swift</h5>
											<h6>Mug</h6>
											<small>by John Dae</small>
										</a>						
									</div>
									<div class="postcard-footer">
										<label>
											<i class="far fa-eye"></i> 100
										</label>
										<label>
											<i class="fas fa-heart"></i> 50
										</label>
										<label>
											<i class="fab fa-twitch"></i> 80
										</label>
									</div>
								</div>
							</div>
							<div class="col-12 col-sm-6 col-md-6 col-lg-3">
								<div class="postcard">					
									<div class="cover100">
										<span>
											Rp 100.000
										</span>
										<img src="assets/img/bantal.jpg">
									</div>
									<div class="postcard-body">
										<a href="">
											<h5>Women</h5>
											<h6>Bantal</h6>
											<small>by John Dae</small>
										</a>						
									</div>
									<div class="postcard-footer">
										<label>
											<i class="far fa-eye"></i> 100
										</label>
										<label>
											<i class="fas fa-heart"></i> 50
										</label>
										<label>
											<i class="fab fa-twitch"></i> 80
										</label>
									</div>
								</div>
							</div>
							<div class="col-12 col-sm-6 col-md-6 col-lg-3">
								<div class="postcard">					
									<div class="cover100">
										<span>
											Rp 120.000
										</span>
										<img src="assets/img/casehp.jpg">
									</div>
									<div class="postcard-body">
										<a href="">
											<h5>OASIS</h5>
											<h6>WPAP</h6>
											<small>by John Dae</small>
										</a>						
									</div>
									<div class="postcard-footer">
										<label>
											<i class="far fa-eye"></i> 100
										</label>
										<label>
											<i class="fas fa-heart"></i> 50
										</label>
										<label>
											<i class="fab fa-twitch"></i> 80
										</label>
									</div>
								</div>
							</div>
						</div>

					</div>
					<div class="tab-pane fade" id="v-pills-creator-favorit" role="tabpanel" aria-labelledby="v-pills-creator-favorit-tab">

						<div class="row mb-4">
							<div class="col-6 col-sm-6 col-md-6 col-lg-6 col-xl-6" align="left">
								<h6 class="b-600">Creator Favorit</h6>
							</div>
							<div class="col-6 col-sm-6 col-md-6 col-lg-6 col-xl-6" align="right">
								<div class="btn-yellow">
									<button type="submit" class="btn btn-sm">Hapus Semua </button>
								</div>
							</div>
						</div>

						<div class="row mt-4">
							<div class="col-12 col-sm-6 col-md-6 col-lg-4">
								<a href="creator.php">									
									<figure class="snip0045 red">
										<figcaption>
											<h2>John Dae</h2>
											<p>Some things don't need the thought people give them.</p>
										</figcaption>
										<img src="assets/img/profil3.jpg" alt="sample7"/>   
										<div class="position">Surakarta</div>
									</figure>
								</a>
							</div>
							<div class="col-12 col-sm-6 col-md-6 col-lg-4">
								<a href="creator.php">									
									<figure class="snip0045 red">
										<figcaption>
											<h2>John Dae</h2>
											<p>Some things don't need the thought people give them.</p>
										</figcaption>
										<img src="assets/img/profil4.jpg" alt="sample7"/>   
										<div class="position">Surakarta</div>
									</figure>
								</a>
							</div>
							<div class="col-12 col-sm-6 col-md-6 col-lg-4">
								<a href="creator.php">									
									<figure class="snip0045 red">
										<figcaption>
											<h2>John Dae</h2>
											<p>Some things don't need the thought people give them.</p>
										</figcaption>
										<img src="assets/img/profil2.jpg" alt="sample7"/>   
										<div class="position">Surakarta</div>
									</figure>
								</a>
							</div>
						</div>


					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<?php
include "footer-dashboard.php";
?>
<?php
include "footer.php";
?>